﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace SanchiCommerce.Core
{
    [DataContract]
    [Serializable]
    public class ReviewAndRatingByUser
    {


        public ReviewAndRatingByUser()
        {
        }

        private int _rowTotal;
        private int _rownumber;

        private int _UserReviewID;

        private string _username;

        private string _review;

        private int _rating;

        [DataMember]
        public int RowTotal
        {
            get { return _rowTotal; }
            set { _rowTotal = value; }
        }
        [DataMember]
        public int RowNumber
        {
            get { return _rownumber; }
            set { _rownumber = value; }
        }
        [DataMember]
        public int UserReviewID
        {
            get
            {
                return this._UserReviewID;
            }
            set
            {
                if ((this._UserReviewID != value))
                {
                    this._UserReviewID = value;
                }
            }
        }

        [DataMember]
        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                if ((this._username != value))
                {
                    this._username = value;
                }
            }
        }

        [DataMember]
        public string Review
        {
            get
            {
                return this._review;
            }
            set
            {
                if ((this._review != value))
                {
                    this._review = value;
                }
            }
        }


        [DataMember]
        public int Rating
        {
            get
            {
                return this._rating;
            }
            set
            {
                if ((this._rating != value))
                {
                    this._rating = value;
                }
            }
        }
    }
}
