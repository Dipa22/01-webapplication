﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanchiCommerce.Core
{
    [DataContract]
    [Serializable]
    public class UserRatingAndReview
    {
        [DataMember(Name = "_Rating", Order = 4)]
        private string _Rating;
        [DataMember(Name = "_username", Order = 0)]
        private string _username;
        [DataMember(Name = "_statusID", Order = 1)]
        private int _statusID;
        [DataMember(Name = "_review")]
        private string _review;
        [DataMember(Name = "_USerReviewID")]
        private string _USerReviewID;
        [DataMember(Name = "_rowTotal")]
        private System.Nullable<int> _rowTotal;
        public System.Nullable<int> RowTotal
        {
            get
            {
                return this._rowTotal;
            }
            set
            {
                if ((this._rowTotal != value))
                {
                    this._rowTotal = value;
                }
            }
        }
        public UserRatingAndReview()
        {
        }
        public string Rating
        {
            get
            {
                return this._Rating;
            }
            set
            {
                if ((this._Rating != value))
                {
                    this._Rating = value;
                }
            }
        }
        public string UserReviewID
        {
            get
            {
                return this._USerReviewID;
            }
            set
            {
                if ((this._USerReviewID != value))
                {
                    this._USerReviewID = value;
                }
            }
        }
        public string Review
        {
            get
            {
                return this._review;
            }
            set
            {
                if ((this._review != value))
                {
                    this._review = value;
                }
            }
        }
        public int StatusID
        {
            get
            {
                return this._statusID;
            }
            set
            {
                if ((this._statusID != value))
                {
                    this._statusID = value;
                }
            }
        }
        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                if ((this._username != value))
                {
                    this._username = value;
                }
            }
        }

    }
    public class UserRatingAndReviewBasicInfo
    {

        public string UserName { get; set; }
        public string StatusID { get; set; }


    }
}
