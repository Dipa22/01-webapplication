﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SanchiCommerce.Core;
using SageFrame.Web.Utilities;
using System.Data;

namespace SanchiCommerce.LatestItems
{
    public class AspxLatestItemsProvider
    {
        public AspxLatestItemsProvider()
        {
        }

        public List<LatestItemsInfo> GetLatestItemsByCount(AspxCommonInfo aspxCommonObj, int count)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@Count", count));
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsList<LatestItemsInfo>("[dbo].[usp_Aspx_LatestItemsGetByCount]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataSet GetLatestItemsByCount(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsDataSet("[dbo].[usp_Aspx_GetLatestItemInfo]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public LatestItemSettingInfo GetLatestItemSetting(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPC(aspxCommonObj);
                SQLHandler sqlH = new SQLHandler();
                LatestItemSettingInfo objLatestSetting = new LatestItemSettingInfo();
                objLatestSetting = sqlH.ExecuteAsObject<LatestItemSettingInfo>("[dbo].[usp_Aspx_LatestItemSettingGet]", parameterCollection);
                return objLatestSetting;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void LatestItemSettingUpdate(string SettingValues, string SettingKeys, AspxCommonInfo aspxCommonObj)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSP(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@SettingKeys", SettingKeys));
                parameterCollection.Add(new KeyValuePair<string, object>("@SettingValues", SettingValues));
                SQLHandler sqLH = new SQLHandler();
                sqLH.ExecuteNonQuery("[dbo].[usp_Aspx_LatestItemSettingsUpdate]", parameterCollection);
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public List<LatestItemRssInfo> GetLatestItemRssContent(AspxCommonInfo aspxCommonObj, int count)
        {
            try
            {
                List<LatestItemRssInfo> rssFeedContent = new List<LatestItemRssInfo>();
                List<KeyValuePair<string, object>> Parameter = new List<KeyValuePair<string, object>>();
                Parameter.Add(new KeyValuePair<string, object>("@StoreID", aspxCommonObj.StoreID));
                Parameter.Add(new KeyValuePair<string, object>("@PortalID", aspxCommonObj.PortalID));
                Parameter.Add(new KeyValuePair<string, object>("@CultureName", aspxCommonObj.CultureName));
                Parameter.Add(new KeyValuePair<string, object>("@UserName", aspxCommonObj.UserName));
                Parameter.Add(new KeyValuePair<string, object>("@Count", count));
                SQLHandler SQLH = new SQLHandler();
                rssFeedContent = SQLH.ExecuteAsList<LatestItemRssInfo>("[dbo].[usp_Aspx_GetRssFeedLatestItem]", Parameter);
                return rssFeedContent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet LatestItemWithOptionInfo(AspxCommonInfo aspxCommonObj, int sort)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@SortBy", sort));
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsDataSet("[dbo].[usp_Aspx_LatestItemsWithOptions_GetLatestItemsInfo]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static DataSet SpecialItemWithOptionInfo(AspxCommonInfo aspxCommonObj, int sort)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@SortBy", sort));
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsDataSet("[dbo].[usp_Aspx_SpecialItemsWithOptions_GetSpecialItemsInfo]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static DataSet FaturedItemWithOptionInfo(AspxCommonInfo aspxCommonObj, int sort)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@SortBy", sort));
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsDataSet("[dbo].[usp_Aspx_FeaturedItemsWithOptions_GetFeaturedItemsInfo]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public LatestItemSettingInfo GetLatestItemsOptionSetting(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPC(aspxCommonObj);
                SQLHandler sqlH = new SQLHandler();
                LatestItemSettingInfo objLatestSetting = new LatestItemSettingInfo();
                objLatestSetting = sqlH.ExecuteAsObject<LatestItemSettingInfo>("[dbo].[usp_Aspx_LatestItemWithOptionSettingGet]", parameterCollection);
                return objLatestSetting;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static List<VariantCombination> CheckCostVariantCombinationbyItemID(int itemID, AspxCommonInfo aspxCommonObj, string costVarinatValueIDs) 
        {
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@ItemID", itemID));
                parameterCollection.Add(new KeyValuePair<string, object>("@CostVariantsValueIDs", costVarinatValueIDs));
                SQLHandler sqlH = new SQLHandler();
                return sqlH.ExecuteAsList<VariantCombination>("[dbo].[usp_Aspx_LatestItemsWithOptions_CheckCombinationForCostVariantsByItemID]", parameterCollection);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static List<LatestItemsInfo> GetLatestItemsList(int offset, int limit, int count, int sortBy, AspxCommonInfo aspxCommonObj, string attributes)
        {
            string spName = string.Empty;
            try
            {
                List<KeyValuePair<string, object>> parameter = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameter.Add(new KeyValuePair<string, object>("@offset", offset));
                parameter.Add(new KeyValuePair<string, object>("@limit", limit));
                //parameter.Add(new KeyValuePair<string, object>("@BrandIDs", brandIds));
                parameter.Add(new KeyValuePair<string, object>("@Attributes", attributes));
                //parameter.Add(new KeyValuePair<string, object>("@PriceFrom", priceFrom));
                //parameter.Add(new KeyValuePair<string, object>("@PriceTo", priceTo));
                //parameter.Add(new KeyValuePair<string, object>("@CategoryKey", categoryName));
                //parameter.Add(new KeyValuePair<string, object>("@IsByCategory", isByCategory));
                SQLHandler sqlH = new SQLHandler();
                if (sortBy == 1)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByNewest]";
                }
                else if (sortBy == 2)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByOldest]";
                }
                else if (sortBy == 3)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByMostExpensive]";
                }
                else if (sortBy == 4)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByCheapest]";
                }
                else if (sortBy == 5)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByName]";
                }
                else if (sortBy == 6)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByMostPopular]";
                }
                else if (sortBy == 7)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByIsFeatured]";
                }
                else if (sortBy == 8)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByIsSpecial]";
                }
                else if (sortBy == 9)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByBestSeller]";
                }
                else if (sortBy == 10)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByHighestDiscount]";
                }
                else if (sortBy == 11)
                {
                    spName = "[dbo].[usp_Aspx_LatestItemsWithOptionsSortByHighestRated]";
                }
                List<LatestItemsInfo> lstItemBasic = sqlH.ExecuteAsList<LatestItemsInfo>(spName, parameter);
                return lstItemBasic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<LatestItemsInfo> GetSpecialItemsList(int offset, int limit, AspxCommonInfo aspxCommonObj, int sortBy, string attributes)
        {
            string spName = string.Empty;
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@offset", offset));
                parameterCollection.Add(new KeyValuePair<string, object>("@limit", limit));
                parameterCollection.Add(new KeyValuePair<string, object>("@RowTotal", 0));
                parameterCollection.Add(new KeyValuePair<string, object>("@Attributes", attributes));
                SQLHandler sqlH = new SQLHandler();
                if (sortBy == 1)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByItemIDDesc]";
                }
                else if (sortBy == 2)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByItemIDAsc]";
                }
                else if (sortBy == 3)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByPriceDesc]";
                }
                else if (sortBy == 4)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByPriceAsc]";
                }
                else if (sortBy == 5)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByName]";
                }
                else if (sortBy == 6)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByViewCount]";
                }
                else if (sortBy == 7)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByIsFeatured]";
                }
                else if (sortBy == 8)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByIsSpecial]";
                }
                else if (sortBy == 9)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortBySoldItem]";
                }
                else if (sortBy == 10)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByDiscount]";
                }
                else if (sortBy == 11)
                {
                    spName = "[dbo].[usp_Aspx_GetSpecialItemDetailsSortByRatedValue]";
                }
                List<LatestItemsInfo> lstCatDetail = sqlH.ExecuteAsList<LatestItemsInfo>(spName, parameterCollection);
                return lstCatDetail;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<LatestItemsInfo> GetFeaturedItemsList(int offset, int limit, AspxCommonInfo aspxCommonObj, int sortBy, string attributes)
        {
            string spName = string.Empty;
            try
            {
                List<KeyValuePair<string, object>> parameterCollection = CommonParmBuilder.GetParamSPUC(aspxCommonObj);
                parameterCollection.Add(new KeyValuePair<string, object>("@offset", offset));
                parameterCollection.Add(new KeyValuePair<string, object>("@limit", limit));
                parameterCollection.Add(new KeyValuePair<string, object>("@RowTotal", 0));
                parameterCollection.Add(new KeyValuePair<string, object>("@Attributes", attributes));
                SQLHandler sqlH = new SQLHandler();
                spName = "[dbo].[usp_Aspx_GetFeaturedItemDetailsSortByName]";
                List<LatestItemsInfo> lstCatDetail = sqlH.ExecuteAsList<LatestItemsInfo>(spName, parameterCollection);
                return lstCatDetail;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
