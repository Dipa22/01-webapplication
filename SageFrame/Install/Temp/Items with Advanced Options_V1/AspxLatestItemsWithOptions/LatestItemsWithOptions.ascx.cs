﻿/*
AspxCommerce® - http://www.AspxCommerce.com
Copyright (c) 20011-2012 by AspxCommerce
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
using System;
using SageFrame.Web;
using AspxCommerce.Core;
using System.Web.UI;
using AspxCommerce.LatestItemsWithOptions;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public partial class Modules_AspxLatestItemsWithOptions_LatestItemDisplay : BaseAdministrationUserControl
{
    public string AllowOutStockPurchase, AllowAddToCart, modulePath, AspxLatestItemsWithOptionsModulePath, DefaultImagePath = string.Empty;
    public int NoOfLatestItems, rowTotalToDisplay;
    private Hashtable hst = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Page.ClientScript.RegisterClientScriptInclude("Paging", ResolveUrl("~/js/Paging/jquery.pagination.js"));
                IncludeCss("LatestItemsWithOptions", "/Templates/" + TemplateName + "/css/MessageBox/style.css",
                           "/Modules/AspxCommerce/AspxLatestItemsWithOptions/css/LatestItemList.css",
                           "/Templates/" + TemplateName + "/css/ToolTip/ToolTip.css");
                IncludeJs("LatestItemsWithOptions", "/js/DateTime/date.js",
                          "/js/MessageBox/jquery.easing.1.3.js", "/js/MessageBox/alertbox.js",
                          "/Modules/AspxCommerce/AspxLatestItemsWithOptions/js/LatestItemsWithOptionsLatestItemsList.js",
                          "/js/jquery.tipsy.js", "/js/FancyDropDown/itemFancyDropdown.js");
                BindLatestItemWithOptions();
            }
            IncludeLanguageJS();
        }
        catch (Exception ex)
        {
            ProcessException(ex);
        }
    }

    #region Bind Latest Items With Options
    private void BindLatestItemWithOptions()
    {
        int StoreID, PortalID, CustomerID;
        string CultureName, UserName, SessionCode = string.Empty, sortByOptions, sortByOptionsDefault;
        GetPortalCommonInfo(out StoreID, out PortalID, out CustomerID, out UserName, out CultureName, out SessionCode);
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo(StoreID, PortalID, UserName, CultureName, CustomerID, SessionCode);
        StoreSettingConfig ssc = new StoreSettingConfig();
        ssc.GetStoreSettingParamFive(StoreSetting.AllowOutStockPurchase, StoreSetting.ShowAddToCartButton, StoreSetting.DefaultProductImageURL, StoreSetting.SortByOptions, StoreSetting.SortByOptionsDefault, out AllowOutStockPurchase, out AllowAddToCart, out DefaultImagePath, out sortByOptions, out sortByOptionsDefault, StoreID, PortalID, CultureName);
        string aspxRootPath = ResolveUrl("~/");
        string pageExtension = SageFrameSettingKeys.PageExtension;
        string modulePath = ResolveUrl(this.AppRelativeTemplateSourceDirectory);
        AspxLatestItemsWithOptionsModulePath = ResolveUrl(modulePath);
        hst = AppLocalized.getLocale(modulePath);

        #region Sort Order DropDown
        StringBuilder itemViewSortOptions = new StringBuilder();
        itemViewSortOptions.Append("<h4>");
        itemViewSortOptions.Append(getLocale("Sort by:"));
        itemViewSortOptions.Append("</h4><select id=\"ddlSortItemDetailBy\" class=\"sfListmenu\" autocomplete=\"off\">");
        if (!string.IsNullOrEmpty(sortByOptions))
        {
            sortByOptions = sortByOptions.TrimEnd(',');
            string[] sortOption;
            foreach (string option in sortByOptions.Split(','))
            {
                sortOption = option.Split('#');
                itemViewSortOptions.Append("<option");
                if (sortOption[0].Equals(sortByOptionsDefault))
                {
                    itemViewSortOptions.Append(" selected=\"selected\"");
                }
                itemViewSortOptions.Append(" data-html-text=\"");
                itemViewSortOptions.Append(sortOption[1]);
                itemViewSortOptions.Append("\" value=");
                itemViewSortOptions.Append(sortOption[0]);
                itemViewSortOptions.Append(">");
                itemViewSortOptions.Append(sortOption[1]);
                itemViewSortOptions.Append("</option>");
            }
            itemViewSortOptions.Append("</select>");
            ltrItemViewDetailSortBy.Text = itemViewSortOptions.ToString();
        }
        #endregion

        #region PageSize Pagination
        StringBuilder pageSize = new StringBuilder();
        int pageSizeOptionVal = 0;
        for (int i = 1; i <= 5; i++)
        {
            pageSizeOptionVal = 8 * i;
            pageSize.Append("<option data-html-text=\"");
            pageSize.Append(pageSizeOptionVal);
            pageSize.Append("\" value=\"");
            pageSize.Append(pageSizeOptionVal);
            pageSize.Append("\">");
            pageSize.Append(pageSizeOptionVal);
            pageSize.Append("</option>");
        }
        pageSize.Append("<option data-html-text=\"64\" value=\"64\">64</option>");
        pageSizeOption.Text = pageSize.ToString();
        #endregion

        DataSet dsLatestItemWithOption = AspxLatestItemWithOptionsController.LatestItemWithOptionInfo(aspxCommonObj, Convert.ToInt16(sortByOptionsDefault));
        StringBuilder strBldLatestItemWithOption;
        if (dsLatestItemWithOption != null & dsLatestItemWithOption.Tables.Count == 2)
        {
            #region Latest Item With Option Setting
            DataTable dtLIWOSetting = dsLatestItemWithOption.Tables[0];
            if (dtLIWOSetting != null && dtLIWOSetting.Rows.Count > 0)
            {
                NoOfLatestItems = Convert.ToInt16(dtLIWOSetting.Rows[0]["LatestItemsOptionCount"]);
                string RssFeedUrl = dtLIWOSetting.Rows[0]["LatestItemsOptionRssPage"].ToString();
                if (Convert.ToBoolean(dtLIWOSetting.Rows[0]["EnableLatestItemsOptionRss"].ToString()))
                {
                    StringBuilder strBldRss = new StringBuilder();
                    strBldRss.Append("<a href=\"");
                    strBldRss.Append(aspxRedirectPath);
                    strBldRss.Append(dtLIWOSetting.Rows[0]["LatestItemsOptionRssPage"]);
                    strBldRss.Append(pageExtension);
                    strBldRss.Append("\" class=\"cssRssImage\"><img id=\"heavyDiscountItemRssImage\" alt=\"");
                    strBldRss.Append(getLocale("Latest Items Option"));
                    strBldRss.Append("\" src=\"");
                    strBldRss.Append(aspxRootPath);
                    strBldRss.Append("Templates/");
                    strBldRss.Append(TemplateName);
                    strBldRss.Append("/images/rss-icon.png");
                    strBldRss.Append("\" title=\"");
                    strBldRss.Append(getLocale("Latest Items Option"));
                    strBldRss.Append("\" /></a>");
                    ltrLIWORss.Text = strBldRss.ToString();
                }
            }
            #endregion

            #region Latest Item With Option
            DataTable dtLIWO = dsLatestItemWithOption.Tables[1];
            if (dtLIWO != null && dtLIWO.Rows.Count > 0)
            {
                strBldLatestItemWithOption = new StringBuilder();
                int rowTotal = Convert.ToInt16(dtLIWO.Rows[0]["RowTotal"]);
                rowTotalToDisplay = dtLIWO.Rows.Count;
                int j;
                string imagePath, alternateText, href;
                //To be initialized
                string imageSize;
                string quantityValue, htmlClass;

                StringBuilder strBldDivCostVariant;
                StringBuilder addSpan;
                StringBuilder valueID;
                StringBuilder strBldCostVariantOptions;
                StringBuilder btnWrapper;
                foreach (DataRow drItem in dtLIWO.Rows)
                {
                    imagePath = "";
                    alternateText = ""; href = "";
                    imageSize = "Small";
                    quantityValue = "value=\"1\"";
                    if (!string.IsNullOrEmpty(drItem["ImagePath"].ToString()))
                        imagePath = "Modules/AspxCommerce/AspxItemsManagement/uploads/" + drItem["ImagePath"];
                    else
                        imagePath = DefaultImagePath;
                    alternateText = (!string.IsNullOrEmpty(drItem["AlternateText"].ToString())) ? drItem["AlternateText"].ToString() : drItem["Name"].ToString();
                    href = aspxRedirectPath + "item/" + drItem["SKU"] + pageExtension;

                    strBldLatestItemWithOption.Append("<div class=\"clearfix classInfo\" ><div id=\"product_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" class=\"latest-wrap clearfix\">");
                    strBldLatestItemWithOption.Append("<div  id=\"LOptProductImageWrapID_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" class=\"ItemsImageClass\"><a href=\"");
                    strBldLatestItemWithOption.Append(href);
                    strBldLatestItemWithOption.Append("\"><img class=\"lazy\"  alt=\"");
                    strBldLatestItemWithOption.Append(drItem["AlternateText"]);
                    strBldLatestItemWithOption.Append("\"  title=\"");
                    strBldLatestItemWithOption.Append(alternateText);
                    strBldLatestItemWithOption.Append("\" src=\"");
                    strBldLatestItemWithOption.Append(aspxRootPath);
                    strBldLatestItemWithOption.Append(imagePath.Replace("uploads", "uploads/" + imageSize));
                    strBldLatestItemWithOption.Append("\" /></a>");
                    if (Convert.ToBoolean(drItem["IsFeatured"]))
                    {
                        if (imageSize.Equals("Small"))
                        { strBldLatestItemWithOption.Append("<div class=\"classIsFeatureSmall\"></div>"); }
                        else
                        { strBldLatestItemWithOption.Append("<div class=\"classIsFeatureMedium\"></div>"); }
                    }//isfeatured

                    if (Convert.ToBoolean(drItem["IsSpecial"]))
                    {
                        if (imageSize == "Small")
                        {
                            strBldLatestItemWithOption.Append("<div class=\"classIsSpecialSmall\"></div>");
                        }
                        else
                        {
                            strBldLatestItemWithOption.Append("<div class=\"classIsSpecialMedium\"></div>");
                        }
                    }
                    strBldLatestItemWithOption.Append("</div>");
                    strBldLatestItemWithOption.Append("<div class=\"ItemsInfoClass\"><ul><li><h2>");
                    strBldLatestItemWithOption.Append(drItem["Name"]);
                    strBldLatestItemWithOption.Append("</h2></li><li class=\"cssClassAvailability\"><span>");
                    strBldLatestItemWithOption.Append(getLocale("Availability:"));
                    strBldLatestItemWithOption.Append("</span><b><span id=\"spanAvailability_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\">");
                    if (Convert.ToBoolean(drItem["IsOutOfStock"]))
                    {
                        strBldLatestItemWithOption.Append(getLocale("Out Of Stock"));
                    }
                    else
                    {
                        strBldLatestItemWithOption.Append(getLocale("In Stock"));
                    }
                    strBldLatestItemWithOption.Append("</span></b></li><li class=\"cssClassProductRealPrice\"><span id=\"spanPrice_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" class=\"cssClassFormatCurrency\">");
                    strBldLatestItemWithOption.Append(Math.Round((decimal)drItem["Price"], 2));//
                    strBldLatestItemWithOption.Append("</span><input type=\"hidden\" id=\"hdnPrice_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" value=\"");
                    strBldLatestItemWithOption.Append(Math.Round((decimal)drItem["Price"], 2));
                    strBldLatestItemWithOption.Append("\" /></li></ul>");
                    strBldLatestItemWithOption.Append("<div class=\"classViewDetails\"><a href=\"");
                    strBldLatestItemWithOption.Append(href);
                    strBldLatestItemWithOption.Append("\" ><span>");
                    strBldLatestItemWithOption.Append(getLocale("View Details"));
                    strBldLatestItemWithOption.Append("</span></a></div>");
                    strBldLatestItemWithOption.Append("<div ");
                    strBldDivCostVariant = new StringBuilder();
                    if (!string.IsNullOrEmpty(drItem["CostVariants"].ToString()))
                    {
                        strBldLatestItemWithOption.Append("id=\"divCostVariant_");
                        strBldLatestItemWithOption.Append(drItem["ItemID"]);
                        strBldLatestItemWithOption.Append("\"");

                        quantityValue = " disabled=\"disabled\"";
                        string[] vArray = drItem["CostVariants"].ToString().Split('#');
                        List<CostVariantsWithInputTypeID> cvi = new List<CostVariantsWithInputTypeID>();
                        foreach (string variant in vArray)
                        {
                            string[] fArray = variant.Split(',');
                            cvi.Add(new CostVariantsWithInputTypeID() { CostVariant = new CostVariants() { CostVariantID = Convert.ToInt16(fArray[0]), CostVariantName = fArray[1], CostVariantValueID = Convert.ToInt16(fArray[3]), CostVariantValueName = fArray[4] }, InputTypeID = Convert.ToInt16(fArray[2]) });
                        }

                        foreach (var costVariantGroup in cvi.GroupBy(x => x.CostVariant.CostVariantName))
                        {
                            addSpan = new StringBuilder();
                            addSpan.Append("<div id=\"div_");
                            addSpan.Append(costVariantGroup.First().CostVariant.CostVariantID.ToString());
                            addSpan.Append("_");
                            addSpan.Append(drItem["ItemID"]);
                            addSpan.Append("\" class=\"cssClassCostVariants cssClassHalfColumn_");
                            addSpan.Append(drItem["ItemID"]);
                            addSpan.Append("\">");
                            addSpan.Append("<span id=\"spn_");
                            addSpan.Append(costVariantGroup.First().CostVariant.CostVariantID.ToString());
                            addSpan.Append('_');
                            addSpan.Append(drItem["ItemID"]);
                            addSpan.Append("\" class=\"width50\"><b>");
                            addSpan.Append(costVariantGroup.Key.ToString());
                            addSpan.Append("</b>: ");
                            addSpan.Append("</span>");
                            addSpan.Append("<span class=\"spn_Close_");
                            addSpan.Append(drItem["ItemID"]);
                            addSpan.Append("_");
                            addSpan.Append(costVariantGroup.First().CostVariant.CostVariantID.ToString());
                            addSpan.Append("\"><img class=\"imgDelete\" src=\"");
                            addSpan.Append(aspxRootPath);
                            addSpan.Append("Templates/");
                            addSpan.Append(TemplateName);
                            addSpan.Append("/images/admin/uncheck.png\" title=\"Don\'t use this option\" alt=\"Don\'t use this option\" /></span>");

                            valueID = new StringBuilder();
                            strBldCostVariantOptions = new StringBuilder();
                            htmlClass = "";

                            valueID.Append("controlCostVariant_");
                            valueID.Append(costVariantGroup.First().CostVariant.CostVariantID.ToString());
                            valueID.Append("_");
                            valueID.Append(drItem["ItemID"]);
                            j = 0;
                            foreach (var cv in costVariantGroup)
                            {
                                switch (cv.InputTypeID)
                                {
                                    case 5:
                                        if (j == 0)
                                        {
                                            strBldCostVariantOptions.Append("<select id=\"");
                                            strBldCostVariantOptions.Append(valueID);
                                            strBldCostVariantOptions.Append("\" multiple>");
                                            strBldCostVariantOptions.Append("<option value=\"");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                            strBldCostVariantOptions.Append("\">");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                            strBldCostVariantOptions.Append("</option>");
                                        }
                                        else
                                        {
                                            strBldCostVariantOptions.Append("<option value=\"");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                            strBldCostVariantOptions.Append("\">");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                            strBldCostVariantOptions.Append("</option>");
                                        }
                                        if (j == costVariantGroup.Count() - 1)
                                        {
                                            strBldCostVariantOptions.Append("</select>");
                                            htmlClass = "sfListmenu";
                                        }
                                        break;
                                    case 6:
                                        if (j == 0)
                                        {
                                            strBldCostVariantOptions.Append("<select id=\"");
                                            strBldCostVariantOptions.Append(valueID);
                                            strBldCostVariantOptions.Append("\"><option value=\"none\">");
                                            strBldCostVariantOptions.Append(getLocale("Choose an Option"));
                                            strBldCostVariantOptions.Append("</option>");
                                            strBldCostVariantOptions.Append("<option value=\"");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                            strBldCostVariantOptions.Append("\">");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                            strBldCostVariantOptions.Append("</option>");
                                        }
                                        else
                                        {
                                            strBldCostVariantOptions.Append("<option value=\"");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                            strBldCostVariantOptions.Append("\">");
                                            strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                            strBldCostVariantOptions.Append("</option>");
                                        }
                                        if (j == costVariantGroup.Count() - 1)
                                        {
                                            strBldCostVariantOptions.Append("</select>");
                                            htmlClass = "sfListmenu";
                                        }
                                        break;
                                    case 9:
                                    case 10:
                                        strBldCostVariantOptions.Append("<label><input  name=\"");
                                        strBldCostVariantOptions.Append(valueID);
                                        strBldCostVariantOptions.Append("\" type=\"radio\"  value=\"");
                                        strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                        strBldCostVariantOptions.Append("\" /><span>");
                                        strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                        strBldCostVariantOptions.Append("</span></label>");
                                        htmlClass = "cssClassRadio";
                                        break;
                                    case 11:
                                    case 12:
                                        strBldCostVariantOptions.Append("<input  name=\"");
                                        strBldCostVariantOptions.Append(valueID);
                                        strBldCostVariantOptions.Append("\" type=\"radio\" value=\"");
                                        strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueID.ToString());
                                        strBldCostVariantOptions.Append("\" /><label>");
                                        strBldCostVariantOptions.Append(cv.CostVariant.CostVariantValueName.ToString());
                                        strBldCostVariantOptions.Append("</label></br>");
                                        htmlClass = "cssClassRadio";
                                        break;
                                    default:
                                        break;
                                }
                                j++;
                            }
                            addSpan.Append("<span class=\"");
                            addSpan.Append(htmlClass);
                            addSpan.Append("\" id=\"subDiv");
                            addSpan.Append(costVariantGroup.First().CostVariant.CostVariantID.ToString());
                            addSpan.Append("_");
                            addSpan.Append(drItem["ItemID"]);
                            addSpan.Append("\">");
                            addSpan.Append(strBldCostVariantOptions);
                            addSpan.Append("</span>");
                            addSpan.Append("</div>");
                            strBldDivCostVariant.Append(addSpan);
                        }
                    }//if cost variant not null
                    strBldLatestItemWithOption.Append("class=\"cssClassHalfColtwo\">");
                    //add cost variant div
                    strBldLatestItemWithOption.Append(strBldDivCostVariant);
                    //ends add cost variant div
                    strBldLatestItemWithOption.Append("<div class=\"classQuantity\"><span>");
                    strBldLatestItemWithOption.Append(getLocale("Quantity:"));
                    strBldLatestItemWithOption.Append("</span><input class=\"classQty\" type=\"text\" id=\"qty_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" ");
                    strBldLatestItemWithOption.Append(quantityValue);
                    if (Convert.ToInt16(drItem["ItemTypeID"]) == 3 || Convert.ToInt16(drItem["ItemTypeID"]) == 4 || Convert.ToInt16(drItem["ItemTypeID"]) == 5 || Convert.ToInt16(drItem["ItemTypeID"]) == 6)
                    {
                        strBldLatestItemWithOption.Append("disabled=\"disabled\" ");
                    }
                    strBldLatestItemWithOption.Append("maxlength=\"3\" itemQty=\"");
                    strBldLatestItemWithOption.Append(drItem["Quantity"]);
                    strBldLatestItemWithOption.Append("\" /><label id=\"lblNotification_");
                    strBldLatestItemWithOption.Append(drItem["ItemID"]);
                    strBldLatestItemWithOption.Append("\" style=\"color: #FF0000;\"></label></div>");
                    strBldLatestItemWithOption.Append("</div>");


                    btnWrapper=new StringBuilder();

                    btnWrapper.Append("<div class=\"sfButtonwrapper\">");

                    if (!string.IsNullOrEmpty(drItem["CostVariants"].ToString()))
                    {
                        btnWrapper.Append("<div class=\"cssClassAddtoCard\"><div class=\"\"><label class=\"i-cart cssClassCartLabel cssClassGreenBtn\"><button ");
                        #region Allow Out of Stock Purchase
                        if (!Convert.ToBoolean(AllowOutStockPurchase))
                        {
                            if (Convert.ToBoolean(drItem["IsOutOfStock"]))
                            {
                                btnWrapper.Append("class=\"sBtn cssClassOutOfStock\" id=\"btnAddToMyCart_");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\"  type=\"button\"><span>");
                                btnWrapper.Append(getLocale("Out Of Stock"));
                            }
                            else
                            {
                                btnWrapper.Append("class=\"sBtn addtoCart cssClassAddToCart\" id=\"btnAddToMyCart_");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\" data-addtocart=\"addtocart");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\" type=\"button\" onclick=LatestItemsList.AddToMyCart(");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append(",");
                                btnWrapper.Append(drItem["ItemTypeID"]);
                                btnWrapper.Append(",null");
                                btnWrapper.Append(",");
                                btnWrapper.Append(drItem["Quantity"]);
                                btnWrapper.Append(",\"");
                                btnWrapper.Append(drItem["SKU"]);
                                btnWrapper.Append("\",\"LatestItemsList\",\"true\",this);><span>");
                                btnWrapper.Append(getLocale("Cart+"));
                            }
                        }//if allow out of stockpurchase
                        else
                        {
                            btnWrapper.Append("class=\"sBtn addtoCart cssClassAddToCart\" id=\"btnAddToMyCart_");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append("\" data-addtocart=\"addtocart");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append("\" type=\"button\" onclick=LatestItemsList.AddToMyCart(");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append(",");
                            btnWrapper.Append(drItem["ItemTypeID"]);
                            btnWrapper.Append(",null");
                            btnWrapper.Append(",");
                            btnWrapper.Append(drItem["Quantity"]);
                            btnWrapper.Append(",\"");
                            btnWrapper.Append(drItem["SKU"]);
                            btnWrapper.Append("\",\"LatestItemsList\",\"true\",this);><span>");
                            btnWrapper.Append(getLocale("Cart+"));
                        }
                        #endregion
                        btnWrapper.Append("</span></button></label></div></div>");
                    }//if costvariant is not null
                    else
                    {
                        btnWrapper.Append("<div class=\"cssClassAddtoCard\"><div class=\"\">");
                        #region Allow Out Stock Purchase
                        if (!Convert.ToBoolean(AllowOutStockPurchase))
                        {
                            if (Convert.ToBoolean(drItem["IsOutOfStock"]))
                            {
                                btnWrapper.Append("<button class=\"sBtn cssClassOutOfStock\" id=\"btnAddToMyCart_");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\"  type=\"button\"><span>");
                                btnWrapper.Append(getLocale("Out Of Stock"));
                                btnWrapper.Append("</span></button>");
                            }
                            else
                            {
                                btnWrapper.Append("<label class=\"i-cart cssClassCartLabel cssClassGreenBtn\"><button class=\"sBtn addtoCart cssClassAddToCart\" id=\"btnAddToMyCart_");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\" data-addtocart=\"addtocart");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append("\" type=\"button\" onclick=LatestItemsList.AddToMyCart(");
                                btnWrapper.Append(drItem["ItemID"]);
                                btnWrapper.Append(",");
                                btnWrapper.Append(drItem["ItemTypeID"]);
                                btnWrapper.Append(",null");
                                btnWrapper.Append(",");
                                btnWrapper.Append(drItem["Quantity"]);
                                btnWrapper.Append(",\"");
                                btnWrapper.Append(drItem["SKU"]);
                                btnWrapper.Append("\",\"LatestItemsList\",\"true\",this);><span>");
                                btnWrapper.Append(getLocale("Cart+"));
                                btnWrapper.Append("</span></button></label>");
                            }
                        }//Allow out of stock purchase false
                        else
                        {
                            btnWrapper.Append("<label class=\"i-cart cssClassCartLabel cssClassGreenBtn\"><button class=\"sBtn addtoCart cssClassAddToCart\" id=\"btnAddToMyCart_");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append("\" data-addtocart=\"addtocart");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append("\" type=\"button\" onclick=\"LatestItemsList.AddToMyCart(");
                            btnWrapper.Append(drItem["ItemID"]);
                            btnWrapper.Append(",");
                            btnWrapper.Append(drItem["ItemTypeID"]);
                            btnWrapper.Append(",null");
                            btnWrapper.Append(",");
                            btnWrapper.Append(drItem["Quantity"]);
                            btnWrapper.Append(",\"");
                            btnWrapper.Append(drItem["SKU"]);
                            btnWrapper.Append("\",\"LatestItemsList\",\"true\",this);\"><span>");
                            btnWrapper.Append(getLocale("Cart+"));
                            btnWrapper.Append("</span></button></label>");
                        }//else out of stock purchase false
                        #endregion
                        btnWrapper.Append("</div></div>");
                    }// else cost variant
                    btnWrapper.Append("<div class=\"sfButtonwrapper\">");
                    btnWrapper.Append("<div class=\"cssClassWishListButton\"><label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=");
                    if (GetCustomerID > 0 && GetUsername.ToLower() != "anonymoususer")
                    {
                        btnWrapper.Append("AspxCommerce.RootFunction.CheckWishListUniqueness(");
                        btnWrapper.Append(drItem["ItemID"]);
                        btnWrapper.Append(",'");
                        btnWrapper.Append(drItem["SKU"]);
                        btnWrapper.Append("',this);>");
                    }
                    else
                    {
                        btnWrapper.Append("\"AspxCommerce.RootFunction.Login();\">");
                    }
                    btnWrapper.Append("<span>");
                    btnWrapper.Append(getLocale("Wishlist+"));
                    btnWrapper.Append("</span></button></label></div>");

                    btnWrapper.Append("<div class=\"cssClassCompareButton\"><input type=\"hidden\" name=\"itemcompare\" value=\"");
                    btnWrapper.Append(drItem["ItemID"]);
                    btnWrapper.Append(",'");
                    btnWrapper.Append(drItem["SKU"]);					
                    btnWrapper.Append("',this\" /></div>");	
					btnWrapper.Append("</div></div>");				
                    strBldLatestItemWithOption.Append("</div>");
                    strBldLatestItemWithOption.Append("</div>");
					
					
					strBldLatestItemWithOption.Append(btnWrapper.ToString());
					strBldLatestItemWithOption.Append("</div>");
                }//Ends Foreach Loop
                litLatestItemList.Text = strBldLatestItemWithOption.ToString();
            }//ends if
            else
            {
                strBldLatestItemWithOption = new StringBuilder();
                strBldLatestItemWithOption.Append("<span class=\"cssClassNotFound\"><b>");
                strBldLatestItemWithOption.Append(getLocale("This store has no items listed yet!"));
                strBldLatestItemWithOption.Append("</b></span>");
                litLatestItemList.Text = strBldLatestItemWithOption.ToString();
            }
            #endregion
        }
    }
    #endregion

    #region Localization
    private string getLocale(string messageKey)
    {
        string retStr = string.Empty;
        if (hst != null && hst[messageKey] != null)
        {
            retStr = hst[messageKey].ToString();
        }
        return retStr;
    }
    #endregion
}