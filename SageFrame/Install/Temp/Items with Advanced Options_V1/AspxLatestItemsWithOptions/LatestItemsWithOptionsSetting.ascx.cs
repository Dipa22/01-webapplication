﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SageFrame.Web;
using AspxCommerce.Core;
using System.Web.Script.Serialization;
using AspxCommerce.LatestItemsWithOptions;

public partial class Modules_AspxCommerce_AspxLatestItemsWithOptions_LatestItemsWithOptionsSetting : BaseAdministrationUserControl
{
    public string Settings = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IncludeJs("LatestItemsWithOptionsSetting", "/Modules/AspxCommerce/AspxLatestItemsWithOptions/js/LatestItemsWithOptionsSetting.js", "/js/FormValidation/jquery.validate.js");
            GetLatestItemOptionSetting();

        }
        IncludeLanguageJS();
    }

    #region LatestItemOptionSetting
    private void GetLatestItemOptionSetting()
    {
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo(GetStoreID, GetPortalID, GetCurrentCultureName);
        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        AspxLatestItemWithOptionsController objLatestOption = new AspxLatestItemWithOptionsController();
        LatestItemsOptionSettingInfo objLatestOptionSettingInfo = objLatestOption.GetLatestItemsOptionSetting(aspxCommonObj);
        if (objLatestOptionSettingInfo != null)
        {
            object obj = new
            {
                LatestItemOptionCount = objLatestOptionSettingInfo.LatestItemsOptionCount,
                EnableLatestItemOptionRss = objLatestOptionSettingInfo.EnableLatestItemsOptionRss,
                LatestItemOptionRssCount = objLatestOptionSettingInfo.LatestItemsOptionRssCount,
                LatestItemOptionRssPage = objLatestOptionSettingInfo.LatestItemsOptionRssPage,
                ModuleServicePath = ResolveUrl(this.AppRelativeTemplateSourceDirectory)
            };
            Settings = json_serializer.Serialize(obj);
        }
    }
    #endregion
}