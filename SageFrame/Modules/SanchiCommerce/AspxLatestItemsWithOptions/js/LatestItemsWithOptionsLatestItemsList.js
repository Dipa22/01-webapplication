(function ($) {
    Array.prototype.clean = function (deleteValue) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == deleteValue) {
                this.splice(i, 1);
                i--;
            }
        }
        return this;
    };
    $.LatestItemsWithOptionsLatestItemsList = function (p) {
        //
        p = $.extend
        ({
            noOfLatestItems: 0,
            allowOutStockPurchase: "",
            allowAddToCart: "",
            rowToDisplay: 0,
            aspxLatestItemsWithOptionsModulePath: "",
            DefaultImagePath: ""
        }, p);
        var costVarIDArr = new Array();
        var calledID = new Array();
        var currentpage = 0;
        var arrItemListType = new Array();
        var aspxCommonObj = {
            StoreID: SanchiCommerce.utils.GetStoreID(),
            PortalID: SanchiCommerce.utils.GetPortalID(),
            UserName: SanchiCommerce.utils.GetUserName(),
            CultureName: SanchiCommerce.utils.GetCultureName(),
            SessionCode: SanchiCommerce.utils.GetSessionCode(),
            CustomerID: SanchiCommerce.utils.GetCustomerID()
        };
        var items_per_page = '';
        var defaultItemImage = '';
        var Items = {
            CostVariantID: 0,
            CostVariantName: '',
            InputTypeID: 0,
            CostVariantsValueID: 0,
            CostVariantsValueName: '',
            CostVariantsPriceValue: 0.0,
            CostVariantsWeightValue: 0.0
        };
        var CombinationValues = '';
        IsExistedCategory = function (arr, val) {
            var isExist = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == val) {
                    isExist = true;
                    break;
                }
            }
            return isExist; 3
        };
        var addToCartProperties = {
            onComplete: function (e) {
                if (e) {
                    window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                }
            }
        };
        var url = window.location.href;
        var QueryString = url.substring(url.indexOf('id=') + 3);
        LatestItemsList = {
            config: {
                isPostBack: false,
                async: false,
                cache: false,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{}',
                dataType: 'json',
                baseURL: p.aspxLatestItemsWithOptionsModulePath + "LatestItemsHandler.ashx/",
                method: "",
                url: "",
                userName: SanchiCommerce.utils.GetUserName(),
                ajaxCallMode: 0,
                itemid: 0
            },
            ajaxCall: function (config) {
                $.ajax({
                    type: LatestItemsList.config.type,
                    contentType: LatestItemsList.config.contentType,
                    cache: LatestItemsList.config.cache,
                    async: LatestItemsList.config.async,
                    url: LatestItemsList.config.url,
                    data: LatestItemsList.config.data,
                    dataType: LatestItemsList.config.dataType,
                    success: LatestItemsList.ajaxSuccess,
                    error: LatestItemsList.ajaxFailure
                });
            },
            init: function () {

                $('#ddlLatestPageSize').MakeFancyItemDropDown();
                $("#divlatestItemsList").show();
                $('#ddlLatestSortBy').MakeFancyItemDropDown();
                LatestItemsList.BindEventsOnLoad();//true
                LatestItemsList.GetLatestItems(1, 9, 0, 5);
                $("#ddlLatestPageSize").change(function () {
                    var items_per_page = $(this).val();
                    var offset = 1;
                    LatestItemsList.GetLatestItems(offset, items_per_page, 0, $("#ddlSortItemDetailBy option:selected").val());//ddlLatestSortBy
                });
                $("#ddlSortItemDetailBy").change(function () {
                    var items_per_page = $('#ddlLatestPageSize').val();
                    var offset = 1;
                    LatestItemsList.GetLatestItems(offset, items_per_page, 0, $(this).val());
                });
            
                if (p.rowToDisplay > 0) {
                    var Items_per_page = $('#ddlLatestPageSize').val();
                    $('#liwoPagination').pagination(p.noOfLatestItems, {
                        items_per_page: Items_per_page,
                        current_page: currentpage,
                        callfunction: true,
                        function_name: { name: LatestItemsList.GetLatestItems, limit: $('#ddlLatestPageSize').val(), sortBy: $('#ddlSortItemDetailBy').val() },
                        prev_text: "Prev",
                        next_text: "Next",
                        prev_show_always: false,
                        next_show_always: false
                    });
                }
            },
            GetLatestItems: function (offset, limit, currenpage, sortBy) {
                //NewItemArray.push($.cookie("cityname")!=null?$.cookie("cityname").replace('%40','@'):null);
             
                var attributes = $.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : $.cookie("defaultcityname") != null ? $.cookie("defaultcityname").replace('%40', '@') : "";
                currentpage = currenpage;
                LatestItemsList.config.method = "LatestItemsList";
                LatestItemsList.config.url = LatestItemsList.config.baseURL + LatestItemsList.config.method;
                LatestItemsList.config.data = JSON2.stringify({ offset: offset, limit: limit, count: p.noOfLatestItems, sortBy: sortBy, aspxCommonObj: aspxCommonObj, attributes: attributes });
                LatestItemsList.config.ajaxCallMode = 1;
                LatestItemsList.ajaxCall(LatestItemsList.config);
            },
            ItemViewList: function (appendDiv, pageSize, paging, pageNum, data, arrItemListType, allowOutStockPurchase, varFunction, currentpage, mainVar, costVarIDArr, calledID, sortByID, imageSize) {
      
                arrItemListType.length = 0;

                if (data.d.length > 0) {
                    //
                    var storeId = SanchiCommerce.utils.GetStoreID();
                    var portalId = SanchiCommerce.utils.GetPortalID();
                    var userName = SanchiCommerce.utils.GetUserName();
                    var cultureName = SanchiCommerce.utils.GetCultureName();
                    var customerId = SanchiCommerce.utils.GetCustomerID();
                    var ip = SanchiCommerce.utils.GetClientIP();
                    var sessionCode = SanchiCommerce.utils.GetSessionCode();
                    var userFriendlyURL = SanchiCommerce.utils.IsUserFriendlyUrl();
                    var dropDownID = '';
                    $('#' + appendDiv + '').html('');
                    $.each(data.d, function (index, value) {
                        //
                        var html = '';
                        rowTotal = value.RowTotal;
                        arrItemListType.push(value.ItemID);
                        var hrefItem = aspxRedirectPath + "item/" + fixedEncodeURIComponent(value.SKU) + ".aspx";
                        var imagePath = itemImagePath + value.ImagePath;
                        if (value.ImagePath == "") {
                            imagePath = "";
                        }
                        if (value.AlternateText == "" || value.AlternateText == null) {
                            value.AlternateText = value.Name;
                        }
                        html += '<div class=\"cssClassProductsBox\">';
                        html += '<div id="product_' + value.ItemID + '" class="latest-wrap clearfix"><div  id="LOptProductImageWrapID_' + value.ItemID + '" class="ItemsImageClass cssClassProductPicture"><a href="' + hrefItem + '"><img class="lazy"  alt="' + value.AlternateText + '"  title="' + value.AlternateText + '" src=' + aspxRootPath + imagePath.replace('uploads', 'uploads/' + imageSize + '') + ' /></a>';
                        if (value.IsFeatured == "Yes" || value.IsFeatured == "True") {
                            if (imageSize == "Small") {
                                html += '<div class="classIsFeatureSmall"></div>';
                            }
                            else {
                                html += '<div class="classIsFeatureMedium"></div>';
                            }
                            if (value.IsSpecial == "Yes" || value.IsSpecial == "True") {
                                if (imageSize == "Small") {
                                    html += '<div class="classIsSpecialSmall"></div>';
                                } else {
                                    html += '<div class="classIsSpecialMedium"></div>';
                                }
                            }
                        } else {
                            if (value.IsSpecial == "Yes" || value.IsSpecial == "True") {
                                if (imageSize == "Small") {
                                    html += '<div class="classIsSpecialSmall"></div>';
                                } else {
                                    html += '<div class="classIsSpecialMedium"></div>';
                                }
                            }
                        }
                        html += "</div>"


                        //html += '</div></div>';

                        //upto here
                        var availibility = getLocale(LatestItemWithOptions, 'Out Of Stock');

                        if (!value.IsOutOfStock) {
                            availibility = getLocale(LatestItemWithOptions, 'In Stock');

                        }
                        //
                        html += '<div class="ItemsInfoClass"><ul><li><h2>' + value.Name + '</h2></li><li class="cssClassAvailability"><span>' + getLocale(LatestItemWithOptions, "Availability: ") + '</span><b><span id="spanAvailability_' + value.ItemID + '">' + availibility + '<span></b></li><li class="cssClassProductRealPrice "><span id="spanPrice_' + value.ItemID + '" class="cssClassFormatCurrency">' + (value.Price).toFixed(2) + '</span><input type="hidden"  id="hdnPrice_' + value.ItemID + '"></li></ul>';
                        html += '<div style="display:none" class="classViewDetails"> <a href="' + hrefItem + '" ><span>' + getLocale(LatestItemWithOptions, 'View Details') + '</span></a> </div>';

                        if (value.CostVariants != '' || value.CostVariants != null) {
                            html += '<div style="min-height: 23px;margin: 10px 0 0 0;" id="divCostVariant_' + value.ItemID + '" class="cssClassHalfColtwo">';

                            html += '</div>';
                        }
                        //html += '</div>';
                        html += '<div  class=\"sfButtonwrapper\">';
                        html += "<div class=\"cssClassAddtoCard\"><div class=\"classQuantity\"><span>" + getLocale(LatestItemWithOptions, 'Quantity:') + "</span><input class=\"classQty\" value=\"1\" type=\"text\" id=\"qty_" + value.ItemID + "\" maxlength=\"2\" itemQty=\"" + value.Quantity + "\"/><label id=\"lblNotification_" + value.ItemID + "\" style=\"color: #FF0000;\"></label></div><div class=\"addtocarts\">";
                        if (value.CostVariants != '' && value.CostVariants != null) {
                            //
                            if (p.allowOutStockPurchase.toLowerCase() == 'false') {
                                //
                                if (value.IsOutOfStock) {
                                    html += "<label class=' cssClassCartLabel cssClassGreyBtn'><button  class=\"sBtn cssClassOutOfStock\" id='btnAddToMyCart_" + value.ItemID + "'  type=\"button\"><span><span>" + getLocale(LatestItemWithOptions, 'Out Of Stock') + "</span></span></button></label>";
                                } else {
                                    html += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button class=\"sBtn addtoCart cssClassAddToCart\" id='btnAddToMyCart_" + value.ItemID + "' data-addtocart=\"addtocart" + value.ItemID + "\" type=\"button\" onclick='LatestItemsList.AddToMyCart(" + value.ItemID + "," + value.ItemTypeID + "," + value.Weight + "," + value.Quantity + "," + JSON2.stringify(value.SKU) + "," + JSON2.stringify(mainVar) + "," + userFriendlyURL + ",this);'><span>" + getLocale(LatestItemWithOptions, 'Cart +') + "</span></button></label>";
                                }
                            } else {
                                html += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button class=\"sBtn addtoCart cssClassAddToCart\" id='btnAddToMyCart_" + value.ItemID + "' data-addtocart=\"addtocart" + value.ItemID + "\" type=\"button\" onclick='LatestItemsList.AddToMyCart(" + value.ItemID + "," + value.ItemTypeID + "," + value.Weight + "," + value.Quantity + "," + JSON2.stringify(value.SKU) + "," + JSON2.stringify(mainVar) + "," + userFriendlyURL + ",this);'><span>" + getLocale(LatestItemWithOptions, 'Cart +') + "</span></button></label>";
                            }
                        } else {
                            if (p.allowOutStockPurchase.toLowerCase() == 'false') {
                                if (value.IsOutOfStock) {
                                    html += "<label class=' cssClassCartLabel cssClassGreyBtn'><button class=\"sBtn cssClassOutOfStock\" id='btnAddToMyCart_" + value.ItemID + "'  type=\"button\"><span><span>" + getLocale(LatestItemWithOptions, 'Out Of Stock') + "</span></span></button></label>";
                                } else {
                                    html += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button class=\"sBtn addtoCart cssClassAddToCart\" id='btnAddToMyCart_" + value.ItemID + "' data-addtocart=\"addtocart" + value.ItemID + "\" type=\"button\" onclick='LatestItemsList.AddToMyCart(" + value.ItemID + "," + value.ItemTypeID + "," + value.Weight + "," + value.Quantity + "," + JSON2.stringify(value.SKU) + "," + JSON2.stringify(mainVar) + "," + userFriendlyURL + ",this);'><span>" + getLocale(LatestItemWithOptions, 'Cart +') + "</span></button></label>";
                                }
                            } else {
                                html += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button class=\"sBtn addtoCart cssClassAddToCart\" id='btnAddToMyCart_" + value.ItemID + "' data-addtocart=\"addtocart" + value.ItemID + "\" type=\"button\" onclick='LatestItemsList.AddToMyCart(" + value.ItemID + "," + value.ItemTypeID + "," + value.Weight + "," + value.Quantity + "," + JSON2.stringify(value.SKU) + "," + JSON2.stringify(mainVar) + "," + userFriendlyURL + ",this);'><span>" + getLocale(LatestItemWithOptions, 'Cart +') + "</span></button></label>";
                            }
                        }
                        html += "</div></div></div></div></div>";
                        //html += '<div  class=\"sfButtonwrapper\">';
                        //html += "<div class=\"cssClassWishListButton\"><label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=";
                        //if (customerId > 0 && userName.toLowerCase() != "anonymoususer") {

                        //    html += "SanchiCommerce.RootFunction.CheckWishListUniqueness(";
                        //    html += value.ItemID;
                        //    html += ",'";
                        //    html += JSON2.stringify(value.SKU);
                        //    html += "',this);>";
                        //}
                        //else {
                        //    html += "\"SanchiCommerce.RootFunction.Login();\">";
                        //}
                        //html += "<span>";
                        //html += getLocale(LatestItemWithOptions, "Wishlist+");
                        //html += "</span></button></label></div>";
                        //html += "<div class=\"cssClassCompareButton\"><input type=\"hidden\" name=\"itemcompare\"  value='" + value.ItemID + ',' + JSON2.stringify(value.SKU) + ",this' /></div>";
                        //html += '</div>';
                        //html += '';
                        $('#' + appendDiv + '').append(html);


                        $('#hdnPrice_' + value.ItemID).val(value.Price);
                        if (value.CostVariants != '' && value.CostVariants != null) {
                            var vArry = value.CostVariants.split('#');
                            var CostVariant = [];
                            var variantValue = [];
                            var variantId = [];
                            var CostVariantIdsArr = [];
                            $.each(vArry, function (index, item) {
                                var fArray = item.split(',');
                                Items.CostVariantID = fArray[0];
                                Items.CostVariantName = fArray[1];
                                Items.InputTypeID = fArray[2];
                                Items.CostVariantsValueID = fArray[3];
                                Items.CostVariantsValueName = fArray[4];
                                variantId.push(fArray[0]);
                                if (CostVariant.indexOf(Items.CostVariantID) == -1) {
                                    CostVariant.push(Items.CostVariantID);
                                    var addSpan = '';
                                    addSpan += '<div id="div_' + Items.CostVariantID + '_' + value.ItemID + '" class="cssClassCostVariants cssClassHalfColumn_' + value.ItemID + '">';
                                    addSpan += '<span style="display:none" id="spn_' + Items.CostVariantID + '_' + value.ItemID + '" ><b>' + Items.CostVariantName + '</b>: ' + '</span>';
                                    addSpan += '<span  style="display:none" class="spn_Close_' + value.ItemID + "_" + Items.CostVariantID + '"><img class="imgDelete" src="' + aspxTemplateFolderPath + '/images/admin/uncheck.png" title="' + "Don\'t use this option" + '" alt="' + "Don\'t use this option" + '"/></span>';
                                    addSpan += '</div>';
                                    $('#divCostVariant_' + value.ItemID + '').append(addSpan);
                                }
                                var valueID = '';
                                var ItemsCostValueName = '';
                                if (Items.CostVariantsValueID != -1) {
                                    if (Items.InputTypeID == 5 || Items.InputTypeID == 6) {
                                        if (CostVariantIdsArr.indexOf(Items.CostVariantID + "_" + value.ItemID + '') == -1) {
                                            CostVariantIdsArr.push(Items.CostVariantID + "_" + value.ItemID + '');
                                            ItemsCostValueName += '<span class="sfListmenu" id="subDiv' + Items.CostVariantID + '_' + value.ItemID + '">';
                                            valueID = 'controlCostVariant_' + Items.CostVariantID + '_' + value.ItemID + '';
                                            ItemsCostValueName += LatestItemsList.CreateControl(Items, valueID, false, Items.CostVariantName);
                                            ItemsCostValueName += "</span>";
                                            $('#div_' + Items.CostVariantID + '_' + value.ItemID + '').append(ItemsCostValueName);
                                        }
                                        if (!IsExistedCategory(variantValue, Items.CostVariantsValueID)) {
                                            variantValue.push(Items.CostVariantsValueID);
                                            optionValues = LatestItemsList.BindInsideControl(Items, valueID);
                                            $('#controlCostVariant_' + Items.CostVariantID + '_' + value.ItemID + '').append(optionValues);
                                        }
                                        $('#controlCostVariant_' + Items.CostVariantID + '_' + value.ItemID + 'option:first-child').attr("selected", "selected");
                                    } else {
                                        if ($('#subDiv' + Items.CostVariantID + "_" + value.ItemID + '').length == 0) {
                                            ItemsCostValueName += '<span class="cssClassRadio" id="subDiv' + Items.CostVariantID + "_" + value.ItemID + '">';
                                            valueID = 'controlCostVariant_' + Items.CostVariantID + "_" + value.ItemID + '';
                                            ItemsCostValueName += LatestItemsList.CreateControl(Items, valueID, true, Items.CostVariantName);
                                            ItemsCostValueName += "</span>";
                                            $('#div_' + Items.CostVariantID + "_" + value.ItemID + '').append(ItemsCostValueName);
                                        } else {
                                            valueID = 'controlCostVariant_' + Items.CostVariantID + "_" + value.ItemID + '';
                                            ItemsCostValueName += LatestItemsList.CreateControl(Items, valueID, false, Items.CostVariantName);
                                            $('#subDiv' + Items.CostVariantID + "_" + value.ItemID + '').append(ItemsCostValueName);
                                        }
                                    }
                                }
                                $('#divCostVariant_' + value.ItemID + ' select,#divCostVariant_' + value.ItemID + ' input[type=radio],#divCostVariant_' + value.ItemID + ' input[type=checkbox]').unbind().bind("change", function () {
                                    var itemVariantIds = [];
                                    var variants = '';
                                    var isAllSelected = true;
                                    var divId = $(this).parents('.cssClassHalfColtwo').attr('id');
                                    var itemIds = divId.substring(divId.indexOf('_') + 1, divId.length);
                                    $('#lblNotification_' + itemIds + '').html('');
                                    var hasRadio = false;
                                    var hasSelect = false;
                                    if ($(this).parents('.cssClassHalfColtwo').find(".sfListmenu").attr('id') != undefined) {
                                        hasSelect = true;
                                    }
                                    if ($(this).parents('.cssClassHalfColtwo').find(".cssClassRadio").attr('id') != undefined) {

                                        hasRadio = true;
                                    }
                                    if ($(this).parents().is(".sfListmenu")) {
                                        $(this).find("option[value=none]").remove();
                                    }
                                    $('#divCostVariant_' + value.ItemID + ' input[type=radio]:checked').each(function () {
                                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");
                                        if ($(this).val() != '0')
                                            itemVariantIds.push($(this).val());
                                    });
                                    $('#divCostVariant_' + value.ItemID + ' input[type=radio]').each(function () {
                                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");

                                    });
                                    $('#divCostVariant_' + value.ItemID + ' input[type=checkbox]:checked').each(function () {
                                        if ($(this).val() != '0')
                                            itemVariantIds.push($(this).val());
                                    });
                                    $('#divCostVariant_' + value.ItemID + ' select option:selected').each(function () {
                                        if ($(this).val() == 'none') {
                                            isAllSelected = false;
                                        }
                                        if ($(this).val() != '0' && $(this).val() != 'none')
                                            itemVariantIds.push($(this).val());
                                    });
                                    variants = itemVariantIds.join('@');
                                    if (isAllSelected)
                                        LatestItemsList.CheckVariantCombination(value.ItemID, variants, $('#hdnPrice_' + itemIds).val(), $('#spanPrice_' + itemIds), $('#LOptProductImageWrapID_' + itemIds), $('#spanAvailability_' + itemIds), itemIds);
                                    var cookieCurrency = Currency.cookie.read();
                                    var currentCurrency = '';
                                    if (cookieCurrency == null || cookieCurrency == BaseCurrency) {
                                        currentCurrency = BaseCurrency;
                                    }
                                    else {
                                        currentCurrency = cookieCurrency;
                                    }
                                    $("#spanPrice_" + value.ItemID).removeAttr("data-currency");
                                    $("#spanPrice_" + value.ItemID).removeAttr("data-currency-" + currentCurrency + "");
                                    Currency.currentCurrency = currentCurrency;
                                    Currency.format = 'money_format';
                                    var newCurrency = cookieCurrency;
                                    Currency.convertAll(Currency.currentCurrency, newCurrency);
                                });
                                $(".spn_Close_" + value.ItemID + "_" + Items.CostVariantID + "").find(".imgDelete").unbind().bind("click", function () {
                                    var isAllSelected = true;
                                    $(this).parents('div:first').find("select").find("option[value=none]").remove();
                                    $(this).parents('div:first').find(" input[type=radio]").removeAttr('checked');
                                    if ($(this).parents('div:first').find("select").find("option[value=0]").length == 0) {
                                        $(this).parents('div:first').find('.itemSelect').remove();
                                        var options = $(this).parents('div:first').find("select").html();
                                        var noOption = "<option value=0 >Not required</option>";
                                        $(this).parents('div:first').find("select").html(noOption + options);
                                    } else {
                                        $(this).parents('div:first').find('.itemSelect').remove();
                                        $(this).parents('div:first').find("select").find("option[value=0]").attr('selected', 'selected');
                                    }
                                    if ($(this).parents('div:first').find("select").length > 0) {
                                        var id = $(this).parents('div:first').find("select").attr('id');
                                    }
                                    var itemVariantIds = [];
                                    var variants = '';
                                    var divId = $(this).parents('.cssClassHalfColtwo').attr('id');
                                    var itemIds = divId.substring(divId.indexOf('_') + 1, divId.length);
                                    $('#lblNotification_' + itemIds + '').html('');
                                    $('#divCostVariant_' + value.ItemID + ' input[type=radio]:checked').each(function () {
                                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");
                                        if ($(this).val() != '0')
                                            itemVariantIds.push($(this).val());
                                    });
                                    $('#divCostVariant_' + value.ItemID + ' input[type=checkbox]:checked').each(function () {
                                        if ($(this).val() != '0')
                                            itemVariantIds.push($(this).val());
                                    });
                                    $('#divCostVariant_' + value.ItemID + ' select option:selected').each(function () {
                                        if ($(this).val() == 'none') {
                                            isAllSelected = false;
                                        }
                                        if ($(this).val() != '0' && $(this).val() != 'none')
                                            itemVariantIds.push($(this).val());
                                    });
                                    $('#divCostVariant_' + value.ItemID + ' input[type=radio]').each(function () {
                                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");

                                    });
                                    variants = itemVariantIds.join('@');
                                    if (isAllSelected)
                                        LatestItemsList.CheckVariantCombination(value.ItemID, variants, $('#hdnPrice_' + itemIds).val(), $('#spanPrice_' + itemIds), $('#LOptProductImageWrapID_' + itemIds), $('#spanAvailability_' + itemIds), itemIds);

                                });

                            });
                        }

                        if (value.ItemTypeID == 3 || value.ItemTypeID == 4 || value.ItemTypeID == 5 || value.ItemTypeID == 6) {
                            $("#qty_" + value.ItemID + "").attr("disabled", "disabled");
                        }
                        var cookieCurrency = Currency.cookie.read();
                        var currentCurrency = '';
                        if (cookieCurrency == null || cookieCurrency == BaseCurrency) {
                            currentCurrency = BaseCurrency;
                        }
                        else {
                            currentCurrency = cookieCurrency;
                        }
                        $("#spanPrice_" + value.ItemID).removeAttr("data-currency");
                        $("#spanPrice_" + value.ItemID).removeAttr("data-currency-" + currentCurrency + "");
                        Currency.currentCurrency = currentCurrency;
                        Currency.format = 'money_format';
                        var newCurrency = cookieCurrency;
                        Currency.convertAll(Currency.currentCurrency, newCurrency);
                    });
                    LatestItemsList.BindEventsOnLoad();//false
                    $('.tipsy').remove();
                    $('.ItemsImageClass a img[title]').tipsy({ gravity: 'n' });
                    var Items_per_page = $('#ddlLatestPageSize').val();
                    p.noOfLatestItems = p.noOfLatestItems > data.d[0].RowTotal ? data.d[0].RowTotal : p.noOfLatestItems;
                    $('#liwoPagination').pagination(data.d[0].RowTotal, {
                        items_per_page: Items_per_page,
                        current_page: currentpage,
                        callfunction: true,
                        function_name: { name: LatestItemsList.GetLatestItems, limit: $('#ddlLatestPageSize').val(), sortBy: $('#ddlSortItemDetailBy').val() },
                        prev_text: "Prev",
                        next_text: "Next",
                        prev_show_always: false,
                        next_show_always: false
                    });
                }


                else {
                    p.rowToDisplay = 0;
                    p.noOfLatestItems = 0;
                    $('#liwoPagination').pagination(0, {
                        items_per_page: 0,
                        current_page: 0,
                        callfunction: false,
                        function_name: { name: LatestItemsList.GetLatestItems, limit: $('#ddlLatestPageSize').val(), sortBy: $('#ddlSortItemDetailBy').val() },
                        prev_text: "Prev",
                        next_text: "Next",
                        prev_show_always: false,
                        next_show_always: false
                    });
                    $('#divlatestItemsList .itemList #divSearchPageNumber').hide();
                   var msg = '<span class="cssClassNotFound"><b>' + getLocale(LatestItemWithOptions, "This store has no items listed yet!") + '</b></span>';
                   $('#' + appendDiv).html(msg);
                }

                //$(".itemList").wrapInner("<div class='cssLatestItemContainer'></div>");

                //var x = 0;
                //var divtags = $('.itemList .cssClassProductsBox ');
                //var htmlStr = "";
                //var parentDiv = "";
                //var wrapAllIdenfier = 1;

                //$('.itemList .cssClassProductsBox ').each(function () {
                //    x++;

                //    if ((x % 3) == 0) {
                //        // $(this).addClass('cssClassNoMargin');
                //        $(this).addClass('cssClassNoMargin').addClass('i' + wrapAllIdenfier);
                //        $('.i' + wrapAllIdenfier).wrapAll("<div class='cssLatestItemContainer'></div>");
                //        wrapAllIdenfier++;
                //    }
                //    else $(this).addClass('i' + wrapAllIdenfier)
                //});
                //if (data.d.length > $('.cssLatestItemContainer').length * 3) { $('.i' + wrapAllIdenfier).wrapAll("<div class='cssLatestItemContainer'></div>"); };


                var x = 0;
                var divtags = $('.itemList .cssClassProductsBox ');
                var htmlStr = "";
                var parentDiv = "";
                var wrapAllIdenfier = 1;
                $('.itemList .cssClassProductsBox ').each(function () {
                    x++;

                    if ((x % 3) == 0) {
                        // $(this).addClass('cssClassNoMargin');
                        $(this).addClass('cssClassNoMargin').addClass('i' + wrapAllIdenfier);
                        $('.i' + wrapAllIdenfier).wrapAll("<div class='cssLatestItemContainer cssClassLatestItemList'></div>");
                        wrapAllIdenfier++;
                    }
                    else $(this).addClass('i' + wrapAllIdenfier)
                });
                if (data.d.length > $('.cssLatestItemContainer').length * 3) { $('.i' + wrapAllIdenfier).wrapAll("<div class='cssLatestItemContainer cssClassLatestItemList'></div>"); };




                var $container = $(".cssLatestItemContainer");
                $container.imagesLoaded(function () {
                    $container.masonry({
                        itemSelector: '.cssClassProductsBox',
                        EnableSorting: false
                    });
                });


            },
            BindEventsOnLoad: function () {
                $('[id^="divCostVariant_"] select, [id^="divCostVariant_"] input[type=radio], [id^="divCostVariant_"] input[type=checkbox]').off().on("change", function (e) {
                    var itemVariantIds = [], variants = '', isAllSelected = true, divId = $(this).parents('.cssClassHalfColtwo').attr('id'),
                        itemIds = divId.substring(divId.indexOf('_') + 1, divId.length), hasRadio = false, hasSelect = false;
                    $('#lblNotification_' + itemIds + '').html('');
                    if ($(this).parents('.cssClassHalfColtwo').find(".sfListmenu").attr('id') != undefined) {
                        hasSelect = true;
                    }
                    if ($(this).parents('.cssClassHalfColtwo').find(".cssClassRadio").attr('id') != undefined) {
                        hasRadio = true;
                    }
                    $('#divCostVariant_' + itemIds + ' input[type=radio]:checked').each(function () {
                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");
                        if ($(this).val() != '0')
                            itemVariantIds.push($(this).val());
                    });
                    $('#divCostVariant_' + itemIds + ' input[type=radio]').each(function () {
                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");

                    });
                    $('#divCostVariant_' + itemIds + ' input[type=checkbox]:checked').each(function () {
                        if ($(this).val() != '0')
                            itemVariantIds.push($(this).val());
                    });
                    $('#divCostVariant_' + itemIds + ' select option:selected').each(function () {
                        if ($(this).val() == 'none') {
                            isAllSelected = false;
                        }
                        if ($(this).val() != '0' && $(this).val() != 'none')
                            itemVariantIds.push($(this).val());
                    });
                    variants = itemVariantIds.join('@');
                    if (isAllSelected)
                        LatestItemsList.CheckVariantCombination(itemIds, variants, $('#hdnPrice_' + itemIds).val(), $('#spanPrice_' + itemIds), $('#LOptProductImageWrapID_' + itemIds), $('#spanAvailability_' + itemIds), itemIds);
                    var cookieCurrency = Currency.cookie.read();
                    var currentCurrency = '';
                    if (cookieCurrency == null || cookieCurrency == BaseCurrency) {
                        currentCurrency = BaseCurrency;
                    }
                    else {
                        currentCurrency = cookieCurrency;
                    }
                    $("#spanPrice_" + itemIds).removeAttr("data-currency");
                    $("#spanPrice_" + itemIds).removeAttr("data-currency-" + currentCurrency + "");
                    Currency.currentCurrency = currentCurrency;
                    Currency.format = 'money_format';
                    var newCurrency = cookieCurrency;
                    Currency.convertAll(Currency.currentCurrency, newCurrency);
                });
                //
                $("span[class^='spn_Close_']").find(".imgDelete").off().on("click", function () {
                    var isAllSelected = true;
                    $(this).parents('div:first').find("select").find("option[value=none]").remove();
                    $(this).parents('div:first').find(" input[type=radio]").removeAttr('checked');
                    if ($(this).parents('div:first').find("select").find("option[value=0]").length == 0) {
                        $(this).parents('div:first').find('.itemSelect').remove();
                        var options = $(this).parents('div:first').find("select").html();
                        var noOption = "<option value=0 >Not required</option>";
                        $(this).parents('div:first').find("select").html(noOption + options);
                    } else {
                        $(this).parents('div:first').find('.itemSelect').remove();
                        $(this).parents('div:first').find("select").find("option[value=0]").attr('selected', 'selected');
                    }
                    if ($(this).parents('div:first').find("select").length > 0) {
                        var id = $(this).parents('div:first').find("select").attr('id');
                    }
                    var itemVariantIds = [];
                    var variants = '';
                    var divId = $(this).parents('.cssClassHalfColtwo').attr('id');
                    var itemIds = divId.substring(divId.indexOf('_') + 1, divId.length);
                    $('#lblNotification_' + itemIds + '').html('');
                    $('#divCostVariant_' + itemIds + ' input[type=radio]:checked').each(function () {
                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");
                        if ($(this).val() != '0')
                            itemVariantIds.push($(this).val());
                    });
                    $('#divCostVariant_' + itemIds + ' input[type=checkbox]:checked').each(function () {
                        if ($(this).val() != '0')
                            itemVariantIds.push($(this).val());
                    });
                    $('#divCostVariant_' + itemIds + ' select option:selected').each(function () {
                        if ($(this).val() == 'none') {
                            isAllSelected = false;
                        }
                        if ($(this).val() != '0' && $(this).val() != 'none')
                            itemVariantIds.push($(this).val());
                    });
                    $('#divCostVariant_' + itemIds + ' input[type=radio]').each(function () {
                        $(this).is(":checked") ? $(this).addClass("cssRadioChecked") : $(this).removeClass("cssRadioChecked");

                    });
                    variants = itemVariantIds.join('@');
                    if (isAllSelected)
                        LatestItemsList.CheckVariantCombination(itemIds, variants, $('#hdnPrice_' + itemIds).val(), $('#spanPrice_' + itemIds), $('#LOptProductImageWrapID_' + itemIds), $('#spanAvailability_' + itemIds), itemIds);
                });
                //
                $('[id^="qty_"]').off().on('mouseout', function (e) {
                    var $this = $(this);
                    $this.next('label').html('');
                    if ($this.val() == '') {
                        $this.val('1');
                    }
                });
                $('[id^="qty_"]').off().on('contextmenu', function (e) {
                    $(this).next('label').html('');
                    return false;
                });
                $('[id^="qty_"]').off().on('paste', function (e) {
                    $(this).next('label').html('');
                    e.preventDefault();
                });
                $('[id^="qty_"]').off().on('keypress', function (e) {

                    $(this).next('label').text("").html("");
                    var $this = $(this);
                    var divId = $this.attr('id');
                    var itemIds = divId.substring(divId.indexOf('_') + 1, divId.length);
                    var itemCostVariantIDs = '';
                    var Quantity = eval($this.attr('itemQty'));
                    var CombinationValues = '';
                    if ($("#divCostVariant_" + itemIds + "").length == 0) {
                        itemCostVariantIDs = '0@';
                    } else {
                        $('#divCostVariant_' + itemIds + ' option:selected').each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs += $(this).val() + "@";
                            } else {
                            }
                        });
                        $('#divCostVariant_' + itemIds + ' input[type=radio]:checked').each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs += $(this).val() + "@";
                            } else {
                            }
                        });
                        $('#divCostVariant_' + itemIds + ' input[type=checkbox]:checked').each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs += $(this).val() + "@";
                            } else {
                            }
                        });
                        var param = { itemID: itemIds, aspxCommonObj: aspxCommonObj, costVarinatValueIDs: itemCostVariantIDs };
                        var data = JSON2.stringify(param);
                        $.ajax({
                            type: "POST",
                            url: LatestItemsList.config.baseURL + "CheckCostVariantCombinationbyItemID",
                            data: data,
                            async: false,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {

                                if (msg.d.length > 0) {
                                    $.each(msg.d, function (index, value) {
                                        Quantity = value.CombinationQuantity;
                                        CombinationValues = value.CombinationValues;
                                    });
                                }
                            }
                        });
                    }//
                    if (p.allowOutStockPurchase.toLowerCase() == 'false') {
                        if (Quantity <= 0) {
                            return false;
                        } else {
                            if ((e.which >= 48 && e.which <= 57)) {
                                var num;
                                if (e.which == 48)
                                    num = 0;
                                if (e.which == 49)
                                    num = 1;
                                if (e.which == 50)
                                    num = 2;
                                if (e.which == 51)
                                    num = 3;
                                if (e.which == 52)
                                    num = 4;
                                if (e.which == 53)
                                    num = 5;
                                if (e.which == 54)
                                    num = 6;
                                if (e.which == 55)
                                    num = 7;
                                if (e.which == 56)
                                    num = 8;
                                if (e.which == 57)
                                    num = 9;
                                if ($("#divCostVariant_" + itemIds + "").length == 0) {
                                    var itemQuantityInCart = LatestItemsList.CheckItemQuantityInCart(itemIds, itemCostVariantIDs);
                                    if (itemQuantityInCart != 0.1) { //to test if the item is downloadable or simple(0.1 downloadable)
                                        if ((eval($("#qty_" + itemIds + "").val() + '' + num) + eval(itemQuantityInCart.ItemQuantityInCart)) > eval(Quantity)) {
                                            //$('#lblNotification_' + itemIds + '').html(getLocale(LatestItemWithOptions, " The quantity is greater than the available quantity."));
                                            csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, ' The quantity is greater than the available quantity.') + '</p>');
                                            return false;
                                        }
                                        else {
                                            $('#lblNotification_' + itemIds + '').html('');
                                        }
                                    } else {
                                        $("#qty_" + itemIds + "").val(1).attr("disabled", "disabled");
                                    }
                                }
                                else {

                                    if ((eval($("#qty_" + itemIds + "").val() + '' + num)) > eval(Quantity)) {
                                        //$('#lblNotification_' + itemIds + '').html(getLocale(LatestItemWithOptions, " The quantity is greater than the available quantity."));
                                        csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, ' The quantity is greater than the available quantity.') + '</p>');
                                        return false;
                                    }
                                }

                            }
                        }
                    }

                    if ($(this).val() == "") {
                        if (e.which != 8 && e.which != 0 && (e.which < 49 || e.which > 57)) {
                            return false;
                        }
                    } else {
                        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                            return false;
                        }
                    }
                    if (num != undefined) {
                    }
                });
                $('#divSearchPageNumber').show();
                $('.ItemsImageClass a img[title]').tipsy({ gravity: 'n' });
                $('#ddlLatestPageSize').parent('div').show();
            },
            CreateControl: function (item, controlID, isChecked, CostVariantName) {

                var controlElement = '';
                var costPriceValue = item.CostVariantsPriceValue;
                var weightValue = item.CostVariantsWeightValue;
                if (item.InputTypeID == 5) {
                    controlElement = "<select id='" + controlID + "' multiple></select>";
                } else if (item.InputTypeID == 6) { //DropDown
                    //controlElement = "<select id='" + controlID + "'><option value='none'>" + getLocale(LatestItemWithOptions, 'Choose an Option') + "</option></select>";
                    controlElement = "<select id='" + controlID + "'></select>";
                } else if (item.InputTypeID == 9 || item.InputTypeID == 10) { //Radio //RadioLists
                    controlElement = "<label><input  name='" + controlID + "' type='radio'  value='" + item.CostVariantsValueID + "'><span>" + item.CostVariantsValueName + "</span></label>";

                } else if (item.InputTypeID == 11 || item.InputTypeID == 12) { //CheckBox //CheckBoxLists
                    controlElement = "<input  name='" + controlID + "' type='radio'  value='" + item.CostVariantsValueID + "'><label>" + item.CostVariantsValueName + "</label></br>";

                }
                return controlElement;
            },
            BindInsideControl: function (item, controlID) {
                var optionValues = '';
                var costPriceValue = item.CostVariantsPriceValue;
                var weightValue = item.CostVariantsWeightValue;
                if (item.InputTypeID == 5) {
                    optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";

                } else if (item.InputTypeID == 6) { //DropDown
                    optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";
                }
                return optionValues;
            },
            CheckVariantCombination: function (itemID, costVariantIds, actualPrice, spnPrice, imgWrapDiv, btnAvaialibilty, itemIds) {

                $("#qty_" + itemIds + "").val(1).removeAttr("disabled");
                var param = { itemID: itemID, aspxCommonObj: aspxCommonObj, costVarinatValueIDs: costVariantIds };
                var data = JSON2.stringify(param);
                $.ajax({
                    type: "POST",
                    url: LatestItemsList.config.baseURL + "CheckCostVariantCombinationbyItemID",
                    data: data,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d.length > 0) {
                            $.each(msg.d, function (index, value) {
                                var variantPrice = 0;
                                CombinationValues = value.CombinationValues;
                                if (value.CombinationPriceModifierType) {
                                    variantPrice = eval(actualPrice) * eval(value.CombinationPriceModifier) / 100;
                                } else {
                                    variantPrice = eval(value.CombinationPriceModifier);
                                }
                                totalPrice = parseFloat(actualPrice) + variantPrice;
                                spnPrice.html('').html(totalPrice.toFixed(2));
                                spnPrice.attr("bc", totalPrice.toFixed(2));
                                var imgPath = itemImagePath + value.ImageFile;

                                if ($.trim(value.ImageFile) != "") {
                                    imgWrapDiv.find("img").attr('src', aspxRootPath + imgPath.replace('uploads', 'uploads/Medium'));
                                }
                                if (value.CombinationQuantity > 0) {
                                    $('#spanAvailability_' + itemID + '').text('').text(getLocale(LatestItemWithOptions, 'In Stock'));
                                    $("#btnAddToMyCart_" + itemIds).removeClass("cssClassOutOfStock").addClass('cssClassAddToCard').removeAttr("disabled").attr('enabled', "enabled").find("span>span").html('').html(getLocale(LatestItemWithOptions, "Add to Cart"));

                                }
                                else {
                                    $('#spanAvailability_' + itemID + '').text('').text(getLocale(LatestItemWithOptions, 'Out Of Stock'));

                                    $("#btnAddToMyCart_" + itemIds).removeClass("cssClassAddToCard").addClass('cssClassOutOfStock').attr("disabled", "disabled").find("span>span").html('').html(getLocale(LatestItemWithOptions, "Out Of Stock"));
                                }
                            });
                        }
                        else {
                            $('#spanAvailability_' + itemID + '').text('').text(getLocale(LatestItemWithOptions, 'Not Available'));
                            $("#qty_" + itemIds + "").val(1).attr("disabled", "disabled");
                            imgWrapDiv.find("img").attr('src', aspxRootPath + p.DefaultImagePath.replace('uploads', 'uploads/Small'));
                        }
                    }
                });

            },
            AddToWishlist: function (itemId, sku, thiz) {
                var isCombinationMatched = false;
                var isAllSelected = true;
                if ($('#spanAvailability_' + itemId + '').text() != getLocale(LatestItemWithOptions, 'Not Available')) {
                    isCombinationMatched = true;
                }
                else {
                    isCombinationMatched = false;
                }
                var itemCostVariantIDs = [];
                if ($('#divCostVariant_' + itemId + '').length == 0) {
                    itemCostVariantIDs.push(0);
                    SanchiCommerce.RootFunction.AddToWishList(itemId, '', sku);
                } else {
                    var hasSelect = false;
                    var hasRadio = false;
                    var isChecked = false;
                    if ($(thiz).parents('.classInfo').find('.cssClassHalfColtwo').find(".sfListmenu").attr('id') != undefined) {
                        hasSelect = true;
                    }
                    if ($(thiz).parents('.classInfo').find('.cssClassHalfColtwo').find(".cssClassRadio").attr('id') != undefined) {
                        hasRadio = true;
                    }
                    $('#divCostVariant_' + itemId + ' select option:selected').each(function () {
                        if ($(this).val() == 'none') {
                            isAllSelected = false;
                        }
                        if ($(this).val() != 0 && $(this).val() != 'none') {
                            itemCostVariantIDs.push($(this).val());
                        } else {
                        }
                    });
                    $('#divCostVariant_' + itemId + ' input[type=radio]:checked').each(function () {
                        isChecked = true;
                        if ($(this).val() != 0) {
                            itemCostVariantIDs.push($(this).val());
                        } else {
                        }
                    });

                    $('#divCostVariant_' + itemId + ' input[type=checkbox]:checked').each(function () {
                        if ($(this).val() != 0) {
                            itemCostVariantIDs.push($(this).val());
                        } else {
                        }
                    });
                    if ((hasSelect == true && hasRadio == true) || (hasSelect == true && hasRadio == false)) {
                        if (isAllSelected) {
                            if (isCombinationMatched) {
                                SanchiCommerce.RootFunction.AddToWishList(itemId, CombinationValues, sku);
                            } else {
                                csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose available variants!') + '</p>');
                            }
                        }
                        else {
                            csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose variants option!') + '</p>');
                            return false;
                        }
                    }
                    else if (hasSelect == false && hasRadio == true) {
                        if (isChecked) {
                            if (isCombinationMatched) {
                                SanchiCommerce.RootFunction.AddToWishList(itemId, CombinationValues, sku);
                            } else {
                                csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose available variants!') + '</p>');
                            }
                        }
                        else {
                            csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose variants option!') + '</p>');
                            return false;
                        }
                    }

                }
            },
            AddToMyCart: function (itemID, itemTypeId, itemWeight, itemQuantity, sku, checkVar, userFriendlyURL, thiz) {

                if (itemTypeId == 1) {
                    var isCombinationMatched = true;
                    var PriceModifier = "0.000000000000000000000";
                    var IsPricePercentage = false;
                    var CombinationWeightModifier = "0.0000"
                    var IsWeightPercentage = false;
                    var CombinationQuantity = 0;
                    var CombinationValues = '';
                    var itemCostVariantIDs = [];
                    var isAllSelected = true;
                    if ($.trim($('#qty_' + itemID + '').val()) == "" || $.trim($('#qty_' + itemID + '').val()) <= 0) {
                        csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Invalid quantity.') + '</p>');
                        return false;
                    }
                
                    //if ($('#divCostVariant_' + itemID + '').length == 0) added by hema
                    if ($('#divCostVariant_' + itemID + '').text().length == 0) {
                        var itemQuantityInCart = LatestItemsList.CheckItemQuantityInCart(itemID, "0@");
                        if (itemQuantityInCart != 0.1) { //To know whether the item is downloadable (0.1 downloadable)                
                            if (p.allowOutStockPurchase == 'false') {
                                if (itemQuantity <= 0) {
                                    csscody.alert("<h2>" + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'This product is currently Out Of Stock!') + "</p>");
                                    return false;
                                } else {
                                    var qt = eval($.trim($('#qty_' + itemID + '').val())) + eval(itemQuantityInCart);
                                    if (qt > eval(itemQuantity)) {
                                        csscody.alert("<h2>" + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'This product is currently Out Of Stock!') + "</p>");
                                        return false;
                                    }
                                }
                            }
                        }
                        CombinationValues = "0@";
                    } else {
                        var hasSelect = false;
                        var hasRadio = false;
                        var isChecked = false;
                        if ($(thiz).parents('.classInfo').find('.cssClassHalfColtwo').find(".sfListmenu").attr('id') != undefined) {
                            hasSelect = true;
                        }
                        if ($(thiz).parents('.classInfo').find('.cssClassHalfColtwo').find(".cssClassRadio").attr('id') != undefined) {
                            hasRadio = true;
                        }
                        $('#divCostVariant_' + itemID + ' select option:selected').each(function () {
                            if ($(this).val() == 'none') {
                                isAllSelected = false;
                            }
                            if ($(this).val() != 0 && $(this).val() != 'none') {
                                itemCostVariantIDs.push($(this).val());
                            } else {

                            }
                        });
                        $('#divCostVariant_' + itemID + ' input[type=radio]:checked').each(function () {
                            isChecked = true;
                            if ($(this).val() != 0) {
                                itemCostVariantIDs.push($(this).val());
                            }
                            else {

                            }
                        });

                        $('#divCostVariant_' + itemID + ' input[type=checkbox]:checked').each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs.push($(this).val());
                            } else {
                            }
                        });

                        if (hasSelect == false && hasRadio == true) {
                            isAllSelected = isChecked;
                        }
                        if (isAllSelected) {

                            var param = { itemID: itemID, aspxCommonObj: aspxCommonObj, costVarinatValueIDs: itemCostVariantIDs.join('@') };
                            var data = JSON2.stringify(param);
                            $.ajax({
                                type: "POST",
                                url: LatestItemsList.config.baseURL + "CheckCostVariantCombinationbyItemID",
                                data: data,
                                async: false,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (msg) {
                                    if (msg.d.length > 0) {
                                        $.each(msg.d, function (index, value) {
                                            PriceModifier = value.CombinationPriceModifier;
                                            IsPricePercentage = value.CombinationPriceModifierType;
                                            WeightModifier = value.CombinationWeightModifier;
                                            IsWeightPercentage = value.CombinationWeightModifierType;
                                            CombinationQuantity = value.CombinationQuantity;
                                            CombinationValues = value.CombinationValues + '@';
                                        });
                                    }
                                    else {
                                        isCombinationMatched = false;

                                    }
                                }
                            });
                            if (!isCombinationMatched) {
                                csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose available variants!') + '</p>');
                                return false;
                            }

                            if (p.allowOutStockPurchase == 'false') {
                                if (parseInt(CombinationQuantity) <= 0) {
                                    csscody.alert("<h2>" + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'This product is currently Out Of Stock!') + "</p>");
                                    return false;
                                }
                                else {
                                    if ((eval($.trim($('#qty_' + itemID + '').val())) > eval(CombinationQuantity))) {
                                        csscody.alert("<h2>" + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'This product is currently Out Of Stock!') + "</p>");
                                        return false;
                                    }
                                }

                            }
                        }
                        else {
                            csscody.alert('<h2>' + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'Please choose variants option!') + '</p>');
                            return false;
                        }
                    }

                    var itemPrice = $('#hdnPrice_' + itemID).val();
                    var itemQuantity = $.trim($('#qty_' + itemID + '').val());
                    var weightWithVariant = 0;
                    var totalWeightVariant = 0;
                    var costVariantPrice = 0;
                    var WeightModifier = 0;// added by hema
                    if ($('#divCostVariant_' + itemID + '').length == 0) {
                        totalWeightVariant = itemWeight;
                    }
                    else {
                        if (IsPricePercentage) {
                            costVariantPrice = eval($('#hdnPrice_' + itemID).val()) * eval(PriceModifier) / 100;
                        } else {
                            costVariantPrice = eval(PriceModifier);
                        }
                        if (IsWeightPercentage) {
                            weightWithVariant = eval(itemWeight) * eval(WeightModifier) / 100;
                        } else {
                            weightWithVariant = eval(WeightModifier);
                        }
                        totalWeightVariant = eval(itemWeight) + eval(weightWithVariant);
                        itemPrice = eval(itemPrice) + eval(costVariantPrice);
                    }
                    if (totalWeightVariant == null) {
                        totalWeightVariant = 0;
                    }
                    var AddItemToCartObj = {
                        ItemID: itemID,
                        Price: itemPrice,
                        Weight: totalWeightVariant,
                        Quantity: itemQuantity,
                        CostVariantIDs: CombinationValues,
                        IsGiftCard: false
                    };
                    var giftCardDetail = {
                        Price: $("#hdnPrice").val(),
                        GiftCardTypeId: parseInt($("input[name=giftcard-type]:checked").val()),
                        GiftCardCode: '',
                        GraphicThemeId: parseInt($(".jcarousel-skin ul li a.selected").attr('data-id')),
                        SenderName: $.trim($("#txtgc_senerName").val()),
                        SenderEmail: $.trim($("#txtgc_senerEmail").val()),
                        RecipientName: $.trim($("#txtgc_recieverName").val()),
                        RecipientEmail: $.trim($("#txtgc_recieverEmail").val()),
                        Messege: $.trim($("#txtgc_messege").val())
                    };

                    var paramz = {
                        aspxCommonObj: aspxCommonObj,
                        AddItemToCartObj: AddItemToCartObj,
                        giftCardDetail: {},
                        kitInfo: {}
                    };

                    var data = JSON2.stringify(paramz);

                    $.ajax({
                        type: "POST",
                        url: aspxservicePath + "AspxCommonHandler.ashx/AddItemstoCartFromDetail",
                        data: data,
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            LatestItemsList.AddItemstoCartFromDetail(data, userFriendlyURL, checkVar, itemID);
                        }
                    });

                    var param = { itemID: itemID, costVariantsValueIDs: itemCostVariantIDs.join('@'), aspxCommonObj: aspxCommonObj };
                    var data = JSON2.stringify(param);
                    $.ajax({
                        type: "POST",
                        url: aspxservicePath + "AspxCommonHandler.ashx/CheckItemOutOfStock",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d) {
                                var id = $(thiz).attr("data-addtocart");
                                $("button[data-addtocart=" + id + "]").each(function () {
                                    var $parent = $(this).parent('div');
                                    $parent.addClass("cssClassOutOfStock");
                                    $parent.html('');
                                    $parent.append("<button><span>Out Of Stock</span></button>");
                                });
                            }
                        }
                    });
                }
                else {
                    SanchiCommerce.RootFunction.RedirectToItemDetails(sku);
                }

            },
            AddItemstoCartFromDetail: function (msg, userFriendlyURL, checkVar, itemId) {
                if (msg.d == 1) {
                    var myCartUrl;
                    if (userFriendlyURL) {
                        myCartUrl = myCartURL + pageExtension;
                    } else {
                        myCartUrl = myCartURL;
                    }
                    var addToCartProperties = {
                        onComplete: function (e) {
                            if (e) {
                                window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                            }
                        }
                    };

                    csscody.addToCart('<h2>' + getLocale(LatestItemWithOptions, "Successful Message") + '</h2><p>' + getLocale(LatestItemWithOptions, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);

                }
                else if (msg.d == 2) {
                    if (p.allowOutStockPurchase == 'false') {
                        csscody.alert("<h2>" + getLocale(LatestItemWithOptions, 'Information Alert') + '</h2><p>' + getLocale(LatestItemWithOptions, 'This product is currently Out Of Stock!') + "</p>");
                    }
                    else {
                        var myCartUrl;
                        if (userFriendlyURL) {
                            myCartUrl = myCartURL + pageExtension;
                        } else {
                            myCartUrl = myCartURL;
                        }
                        var addToCartProperties = {
                            onComplete: function (e) {
                                if (e) {
                                    window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                                }
                            }
                        };
                        csscody.addToCart('<h2>' + getLocale(LatestItemWithOptions, "Successful Message") + '</h2><p>' + getLocale(LatestItemWithOptions, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);
                    }
                }
             
                if ($("#divMiniShoppingCart1").offset() != null) {
              
                    $("#CartItemLoader").html('<img src="./Modules/ShoppingCart/image/loader.gif">');
                    var basketX = '';
                    var basketY = '';
                    var Itemid = "LOptProductImageWrapID_" + itemId;
                    var productIDValSplitter = $((Itemid).split("_"));
                    var productIDVal = productIDValSplitter[1];
                    var productX = $("#LOptProductImageWrapID_" + productIDVal).offset().left;
                    var productY = $("#LOptProductImageWrapID_" + productIDVal).offset().top;
                    if ($("#productID_" + productIDVal).length > 0) {
                        basketX = $("#productID_" + productIDVal).offset().left;
                        basketY = $("#productID_" + productIDVal).offset().top;
                    } else {
                        basketX = $("#divMiniShoppingCart1").offset().left;
                        basketY = $("#divMiniShoppingCart1").offset().top;
                    }
                    var gotoX = basketX - productX;
                    var gotoY = basketY - productY;

                    var newImageWidth = $("#LOptProductImageWrapID_" + productIDVal).width() / 5;
                    var newImageHeight = $("#LOptProductImageWrapID_" + productIDVal).height() / 5;

                    $("#LOptProductImageWrapID_" + productIDVal + " img")
                                                .clone()
                                                .prependTo("#LOptProductImageWrapID_" + productIDVal)
                                                .css({ 'position': 'absolute' })
                                                .animate({ opacity: 0.4 }, 50)
                                                .animate({
                                                    opacity: 0.4,
                                                    marginLeft: gotoX,
                                                    marginTop: gotoY,
                                                    width: newImageWidth,
                                                    height: newImageHeight
                                                }, 1500, function () {
                                                    $(this).remove();

                                                    //TODO:: Add jQuery Counter increament HERE :: done
                                                    //  IncreaseMyCartItemCount(); //for header cart count
                                                    HeaderControl.GetCartItemTotalCount(); //for header cart count from database
                                                    if ($("#cartItemCount").offset() != null) {
                                                        ShopingBag.GetCartItemCount(); //for shopping bag counter from database
                                                        //  IncreaseShoppingBagCount(); // for shopping bag counter from static
                                                        ShopingBag.GetCartItemListDetails(); //for details in shopping bag
                                                    }
                                                    $('.divLatestItemList').html('');
                                                    if (checkVar == "LatestItems") {
                                                        //
                                                        LatestItems.GetLatestItems(1, $("#ddlLatestPageSize option:selected").val(), 0, $("#ddlSortItemDetailBy option:selected").val());//ddlLatestSortBy
                                                    }
                                                    if ($("#divDetailsItemsList").length > 0) {
                                                        ItemDetails.LoadViewDetails();
                                                    }
                                                    if (checkVar == "CategoryItemList") {
                                                        categoryDetails.GetDetail(1, $("#ddlPageSize").val(), 0, $("#ddlSortBy").val());
                                                    }
                                                    if (checkVar == "SimpleSearch") {
                                                        ItemList.BindSimpleSearchResultItems(1, $("#ddlSimpleSearchPageSize").val(), 0, $("#ddlSimpleSortBy option:selected").val());
                                                    }
                                                    if ($("#divMiniShoppingCart1").offset() != null) {
                                                        ShoppingCartFlyOver.GetCartItemCount();
                                                        ShoppingCartFlyOver.GetCartItemListDetails();
                                                    }
                                                    if ($("#divCartDetails").length > 0) {
                                                        AspxCart.GetUserCartDetails(); //for binding mycart's tblCartList
                                                    }
                                                    if ($("#divLatestItems").length > 0) {
                                                        LatestItems.GetLatestItems();
                                                    }

                                                    if ($("#divLatestBookItems").length > 0) {
                                                        LatestItemAnimation.GetLatestBookItem();
                                                    }

                                                    if ($('#divlatestItemsList').length > 0) {
                                                        LatestItemsList.GetLatestItems(1, $('#ddlLatestPageSize').val(), 0, $("#ddlSortItemDetailBy option:selected").val());//ddlLatestSortBy
                                                    }
                                                    if ($("#divShowCategoryItemsList").length > 0) {
                                                        categoryDetails.GetDetail(1, $("#ddlPageSize").val(), 0, $("#ddlSortBy").val());
                                                    }
                                                    if ($("#divYouMayAlsoLike").length > 0) {
                                                        ItemDetail.GetYouMayAlsoLikeItemsList();
                                                    }
                                                    if ($("#divShowSimpleSearchResult").length > 0) {
                                                        ItemList.BindSimpleSearchResultItems(1, $("#ddlSimpleSearchPageSize").val(), 0, $("#ddlSimpleSortBy option:selected").val());
                                                    }
                                                    if ($("#divOptionsSearchResult").length > 0) {
                                                        ItemList.BindShoppingOptionResultItems(1, $('#ddlOptionPageSize').val(), 0, $("#ddlOptionSortBy option:selected").val());
                                                    }
                                                    if ($("#divShowTagItemResult").length > 0) {
                                                        var items_per_page = $('#ddlTagItemPageSize').val();
                                                        ViewTagItem.ListTagsItems(1, items_per_page, 0, $("#ddlSortTagItemBy option:selected").val());
                                                    }
                                                    if ($("#divShowAdvanceSearchResult").length > 0) {
                                                        AdvanceSearch.ShowSearchResult(1, $('#ddlPageSize').val(), 0, $("#ddlAdvanceSearchSortBy option:selected").val());
                                                    }
                                                    if ($("#divWishListContent").length > 0) {
                                                        WishList.GetWishItemList(1, $("#ddlWishListPageSize").val(), 0, $("#ddlWishListSortBy option:selected").val());
                                                    }
                                                    if ($("#divRelatedItems").length > 0) {
                                                        YouMayAlsoLike.GetItemRetatedUpSellAndCrossSellList();
                                                    }
                                                    if ($("#productID_" + productIDVal).length > 0) {
                                                        $("#productID_" + productIDVal).animate({ opacity: 0 }, 100);
                                                        $("#productID_" + productIDVal).animate({ opacity: 0 }, 100);
                                                        $("#productID_" + productIDVal).animate({ opacity: 1 }, 100);
                                                        $("#CartItemLoader").empty();

                                                    } else {
                                                        $("#tblCartListItems tr:last").hide();
                                                        $("#tblCartListItems tr:last").show("slow");
                                                        $("#CartItemLoader").empty();
                                                    }
                                                });
                }
                else {
                    HeaderControl.GetCartItemTotalCount(); //for header cart count from database      
                    $('.divLatestItemList').html('');
                    if (checkVar == "LatestItems") {
                        LatestItemsList.GetLatestItems(1, $("#ddlLatestPageSize option:selected").val(), 0, $("#ddlSortItemDetailBy option:selected").val());//ddlLatestSortBy
                    }
                    if ($("#divDetailsItemsList").length > 0) {
                        ItemDetails.LoadViewDetails();
                    }
                    if (checkVar == "CategoryItemList") {
                        categoryDetails.GetDetail(1, $("#ddlPageSize").val(), 0, $("#ddlSortBy").val());
                    }
                    if (checkVar == "SimpleSearch") {
                        ItemList.BindSimpleSearchResultItems(1, $("#ddlSimpleSearchPageSize").val(), 0, $("#ddlSimpleSortBy option:selected").val());
                    }
                    if ($("#cartItemCount").offset() != null) {
                        ShopingBag.GetCartItemCount();
                    }
                }
            },
            CheckItemQuantityInCart: function (id, costVariantIds) {

                var qty = 0;
                var param = { itemID: id, aspxCommonObj: aspxCommonObj, itemCostVariantIDs: costVariantIds };
                var data = JSON2.stringify(param);
                $.ajax({
                    type: "POST",
                    url: aspxservicePath + "AspxCoreHandler.ashx/CheckItemQuantityInCart",
                    data: data,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        qty = msg.d;
                    }
                });
                return qty;
            },
            ajaxSuccess: function (data) {

                switch (LatestItemsList.config.ajaxCallMode) {
                    case 0:
                        break;
                    case 1:
                        LatestItemsList.ItemViewList('divLatestItemList', 'ddlLatestPageSize', 'liwoPagination', 'divSearchPageNumber', data, arrItemListType, p.allowOutStockPurchase, LatestItemsList.GetLatestItems, currentpage, 'LatestItemsList', costVarIDArr, calledID, 'ddlSortItemDetailBy', 'Medium');//ddlLatestSortBy
                        break;
                }
            }
        };
        LatestItemsList.init();
    };
    $.fn.LatestItemsWithOptionsLatestItemsListDetails = function (p) {
        $.LatestItemsWithOptionsLatestItemsList(p);
    };
})(jQuery);