﻿var AspxRssFeedLocale = {
// Rss Feed title that displayed in RssFeed.aspx page
    "SanchiCommerce Latest Items": "SanchiCommerce Latest Items",
    "SanchiCommerce Best sell Items": "SanchiCommerce Best sell Items",
    "SanchiCommerce Special Items": "SanchiCommerce Special Items",
    "SanchiCommerce Feature Items": "SanchiCommerce Feature Items",
    "SanchiCommerce Heavy Discount Items": "SanchiCommerce Heavy Discount Items",
    "SanchiCommerce Service Type Items": "SanchiCommerce Service Type Items",
    "SanchiCommerce Category": "SanchiCommerce Category",
    "SanchiCommerce Popular Tags": "SanchiCommerce Popular Tags",
    "SanchiCommerce New Orders": "SanchiCommerce New Orders",
    "SanchiCommerce New Customers": "SanchiCommerce New Customers",
    "SanchiCommerce New Item Review": "SanchiCommerce New Item Review",
    "SanchiCommerce New Tag": "SanchiCommerce New Tag",
    "SanchiCommerce Low Stock Items": "SanchiCommerce Low Stock Items",
    "Manage ItemsSanchiCommerce Manage Items": "SanchiCommerce Manage Items",
    "SanchiCommerce Popular Brands": "SanchiCommerce Popular Brands",
    "SanchiCommerce Featured Brands": "SanchiCommerce Featured Brands",
    "SanchiCommerce All Brands": "SanchiCommerce All Brands",

   
       "Latest Items Rss Feed Title": "Latest Items Rss Feed",
    "Latest Items Rss Feed Alt": "Latest Items Rss Feed",
       "New Arrivals Rss Feed Title": "New Arrivals Rss Feed",
    "New Arrivals Rss Feed Alt": "New Arrivals Rss Feed",

       "Best Seller Items Rss Feed Title": "Best Seller Items Rss Feed",
    "Best Seller Items Rss Feed Alt": "Best Seller Items Rss Feed",
       "Special Items Rss Feed Title": "Special Items Rss Feed",
    "Special Items Rss Feed Alt": "Special Items Rss Feed",
       "Popular Tag Rss Feed Title": "Popular Tag Rss Feed",
    "Popular Tag Rss Feed Alt": "Popular Tag Rss Feed",
       "Heavy Discount Rss Feed Title": "Heavy Discount Rss Feed",
    "Heavy Discount Rss Feed Alt": "Heavy Discount Rss Feed",
       "Services Rss Feed Title": "Services Rss Feed Title",
    "Services Rss Feed Alt": "Services Rss Feed Alt",
       "Feature items Rss Feed Title": "Feature items Rss Feed",
    "Feature items Rss Feed Alt": "Feature items Rss Feed",
       "Category Rss Feed Title": "Category Rss Feed Title",
    "Category Rss Feed Alt": "Category Rss Feed Alt",
       "Popular Brands Rss Feed Title": "Popular Brands Rss Feed",
    "Popular Brands Rss Feed Alt": "Popular Brands Rss Feed",

    "Featured Brands Rss Feed Title": "Featured Brands Rss Feed",
    "Featured Brands Rss Feed Alt": "Featured Brands Rss Feed",

    "All Brands Rss Feed Title": "All Brands Rss Feed",
    "All Brands Rss Feed Alt": "All Brands Rss Feed"
};