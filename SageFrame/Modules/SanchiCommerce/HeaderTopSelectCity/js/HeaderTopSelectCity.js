﻿var HeaderTopSelectCity = "";
var date = new Date();
date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
$(function () {
    var aspxCommonInfo = {
        StoreID: SageFramePortalID,
        PortalID: SageFramePortalID,
        UserName: SageFrameUserName,
        CultureName: SageFrameCurrentCulture,
        CustomerID: customerID,
        SessionCode: sessionCode
    };

    HeaderTopSelectCity = {
        init: function() {
        $.ajax({
            type: "POST",
            url: aspxservicePath + "AspxCoreHandler.ashx/GetAttributeValues",
            data: JSON2.stringify({ aspxCommonObj: aspxCommonInfo, attributeName: 'City' }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $.cookie("defaultcityname", data.d[0].AttributeID + "@" + data.d[0].InputTypeID + "@" + data.d[0].Value, { expires: date, path: '/' });

                var options = '';
                $.each(data.d, function (index, v) {
                    options += "<option value=" + v.AttributeID + "@" + v.InputTypeID + "@" + v.Value + ">" + v.Value + "</option>";
                });

                $('#ddlHeaderCity').append(options);
                $('#listname').append(options);

                if ($.cookie("cityname") == null || $.cookie("cityname") == undefined) {
                    $("#dialog").dialog({
                        autoOpen: true,
                        modal: true,
                        closeOnEscape: false,
                        beforeclose: function (event, ui) {
                            return false;
                        },
                        show: 'fade',
                        hide: 'fade',
                        open: function () {
                            $('.ui-widget-overlay', this).hide().fadeIn();
                            $('.ui-icon-closethick').bind('click.close', function () {
                                $('.ui-widget-overlay').fadeOut(function () {
                                    $('.ui-icon-closethick').unbind('click.close'); $('.ui-icon-closethick').trigger('click');
                                });
                            }); jQuery('button.ui-dialog-titlebar-close').hide();
                        }
                    });
                    $(".ui-dialog .ui-dialog-titlebar").css("display", "none");
                    $(".ui-dialog").css("position", "absolute");
                    $(".ui-dialog").css("display", "hidden");
                    $('html, body').css('overflowY', 'hidden');
                    $(".ui-dialog").css("top", "160px");
                    $(".ui-dialog").css("z-index", "9999");
                    $(".ui-dialog").css("left", "53%");
                    $(".ui-dialog").css("width", "350px");
                    $(".ui-dialog").css("height", "198px");
                    $(".ui-dialog").css("margin-left", "-207px");
                    $(".ui-dialog").css("background-color", "white");
                    $(".ui-dialog").css("padding", "20px");
                    $(".ui-dialog").css("border", "solid 5px #333333");
                    $(".ui-dialog").css("border-radius", "5px");
                    $(".ui-dialog").css("background", "url('http://cdn.kiranababu.com/HomeBanner/kbhomepagepopup.jpg')no-repeat");
                    $('#btn').click(function () {
                        $('#dialog').dialog('close');
                        window.top.location.reload();//for page refresh
                        $('body').css('overflowY', 'scroll');
                        var eID = document.getElementById("listname");
                        var city = eID.options[eID.selectedIndex].value;
                        $.cookie("cityname", city, { expires: date, path: '/' });
                        if (window.location.pathname.toLowerCase().split('/')[1] != "category") {
                            switch (window.location.pathname.toLowerCase()) {
                                case '/best-sellers.aspx': FeaturedItemsList.GetFeaturedItems(1, 9, 0, 5);
                                    break;
                                case '/we-recommend.aspx': FeaturedItemsList.GetSpecialItems(1, 9, 0, 5);
                                    break;
                                default: LatestItemsList.GetLatestItems(1, 9, 0, 5);
                                    break;
                            }
                        }
                    });
                }
                else {
                    $('#ddlHeaderCity').val($.cookie("cityname"));
                }
            }
        });
    }


    };
  
    setTimeout(function () {  HeaderTopSelectCity.init(); })

});
function CityWiseBanner(cityname) {
  
    var properties = {
        textBoxBtnOk: 'Continue',
        textBoxBtnCancel: 'Cancel',
        onComplete: function (e) {
            if (e) {
                $.cookie("cityname", $('#ddlHeaderCity').val(), { expires: date, path: '/' });
                ShopingBag.ClearCartItems();
            }
            else {
                var attribute = $.cookie("cityname") != null ? $.cookie("cityname").split('@')[2] : '';
                $('#ddlHeaderCity').val(attribute); 
            }
        }
    }

    if ($('#cartItemCount span').text() > 0) {
        csscody.confirm("<h2>Alert</h2><p>City change at this point will clear yor cart.</p>", properties);
      
    }
    else {
        $.cookie("cityname", $('#ddlHeaderCity').val(), { expires: date, path: '/' });
        window.location = '/Default.aspx';
    }
};
