﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SageFrame.Web;
using SanchiCommerce.Core;
using System.Text;
//using SanchiCommerce.Moneybookers;
using SageFrame.Web.Utilities;
using SageFrame.Core;
using System.Security.Cryptography;
using System.Configuration;


public partial class Modules_SanchiCommerce_PayUMoney_PayumoneySuccess : BaseAdministrationUserControl
{
    public string transID, invoice, addressPath, sageRedirectPath = string.Empty, selectedCurrency = string.Empty;
    public bool IsUseFriendlyUrls = true;
    public int orderID;
    public string item;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            IncludeLanguageJS();
            OrderDetailsCollection orderdata = new OrderDetailsCollection();
            orderdata = (OrderDetailsCollection)HttpContext.Current.Session["OrderCollection"];
            try
            {
                string[] merc_hash_vars_seq;
                string merc_hash_string = string.Empty;
                string merc_hash = string.Empty;
                string order_id = string.Empty;
                string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                if (Request.Form["status"] == "success")
                {
                    merc_hash_vars_seq = hash_seq.Split('|');
                    Array.Reverse(merc_hash_vars_seq);
                    merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + Request.Form["status"];
                    foreach (string merc_hash_var in merc_hash_vars_seq)
                    {
                        merc_hash_string += "|";
                        merc_hash_string = merc_hash_string + (Request.Form[merc_hash_var] != null ? Request.Form[merc_hash_var] : "");
                    }
                    //Response.Write(merc_hash_string);
                    merc_hash = Generatehash512(merc_hash_string).ToLower();
                    if (merc_hash != Request.Form["hash"])
                    {
                        //Response.Write("Hash value did not matched");
                    }
                    else
                    {
                        order_id = Request.Form["txnid"];
                        //Response.Write("value matched");
                        //Hash value did not matched
                    }
                    SageFrameConfig sfConfig = new SageFrameConfig();
                    IsUseFriendlyUrls = sfConfig.GetSettingBollByKey(SageFrameSettingKeys.UseFriendlyUrls);
                    if (IsUseFriendlyUrls)
                    {
                        if (GetPortalID > 1)
                        {
                            addressPath = HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + "/portal/" + GetPortalSEOName + "/";
                            sageRedirectPath = ResolveUrl("~/portal/" + GetPortalSEOName + "/" + sfConfig.GetSettingsByKey(SageFrameSettingKeys.PortalDefaultPage) + ".aspx");
                        }
                        else
                        {
                            addressPath = HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + "/";
                            sageRedirectPath = ResolveUrl("~/" + sfConfig.GetSettingsByKey(SageFrameSettingKeys.PortalDefaultPage) + ".aspx");
                        }
                    }
                    else
                    {
                        sageRedirectPath = ResolveUrl("{~/Default.aspx?ptlid=" + GetPortalID + "&ptSEO=" + GetPortalSEOName + "&pgnm=" + sfConfig.GetSettingsByKey(SageFrameSettingKeys.PortalDefaultPage));
                    }
                    hlnkHomePage.NavigateUrl = sageRedirectPath;
                    if (Session["OrderID"] != null)
                    {
                        orderID = int.Parse(Session["OrderID"].ToString());
                        //string sQS;
                        //string[] aQS;
                        //string[] aParam = new string[30];
                        //string pwd = "ebskey";//"2c8998ad6670749e0eb4d897e4d4f840";
                        ////string pwd = "2c8998ad6670749e0eb4d897e4d4f840";
                        //string DR = Request.QueryString["DR"].ToString();
                        //DR = DR.Replace(' ', '+');
                        //sQS = Base64Decode(DR);
                        //DR = RC4.Decrypt(pwd, sQS, false);
                        //aQS = DR.Split('&');
                        //EmailTemplate.SendEmailForReturns(obj, aQS);
                        //for (int i = 0; i < aQS.Length; i++)
                        //{
                        //    aParam[i] = aQS[i].Split('=')[1].ToString();
                        //}
                        //if (aParam[0] == "0")
                        //{
                            Image imgProgress = (Image)UpdateProgress1.FindControl("imgPrgress");
                            if (imgProgress != null)
                            {
                                imgProgress.ImageUrl = GetTemplateImageUrl("ajax-loader.gif", true);
                            }
                            if (Session["OrderID"] != null)
                            {
                                
                                AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
                                string transID = merc_hash_string.Split('|')[14];// aParam[24];
                                if (HttpContext.Current.Session["OrderCollection"] != null)
                                {                                    
                                    invoice = orderdata.ObjOrderDetails.InvoiceNumber.ToString();
                                    orderdata.ObjOrderDetails.OrderStatus = "Pending";
                                    aspxCommonObj.CustomerID = GetCustomerID;
                                    aspxCommonObj.SessionCode = HttpContext.Current.Session.ToString();
                                    aspxCommonObj.StoreID = GetStoreID;
                                    aspxCommonObj.PortalID = GetPortalID;
                                    aspxCommonObj.CultureName = GetCurrentCultureName;
                                    aspxCommonObj.UserName = GetUsername;
                                    AspxGiftCardController.IssueGiftCard(orderdata.LstOrderItemsInfo, orderID, false, aspxCommonObj);
                                    EmailTemplate.SendEmailForOrder(GetPortalID, orderdata, addressPath, TemplateName, transID);
                                }
                                if (orderdata.GiftCardDetail != null && HttpContext.Current.Session["UsedGiftCard"] != null)
                                {   //updating giftcard used in chekout
                                    AspxGiftCardController.UpdateGiftCardUsage(orderdata.GiftCardDetail, orderdata.ObjCommonInfo.StoreID,
                                                         orderdata.ObjCommonInfo.PortalID, orderdata.ObjOrderDetails.OrderID, orderdata.ObjCommonInfo.AddedBy,
                                                         orderdata.ObjCommonInfo.CultureName);
                                    HttpContext.Current.Session.Remove("UsedGiftCard");
                                }
                                for (int i = 0; i < orderdata.LstOrderItemsInfo.Count; i++)
                                {
                                    item += orderdata.LstOrderItemsInfo[i].ItemID + "#" + orderdata.LstOrderItemsInfo[i].Quantity + "#" 
                                        + orderdata.LstOrderItemsInfo[i].OrderID + "#" + orderdata.LstOrderItemsInfo[i].Variants+",";
                                    
                                }
                                //string[] itemFields = aParam[22].Split('|');
                                string itemFields;
                                if (orderdata.LstOrderItemsInfo.Count > 1)
                                {  itemFields = item.TrimEnd(','); }
                                else {  itemFields = item; }
                                string responsereason = orderdata.ObjOrderDetails.ResponseReasonText;//aParam[1];
                                //string[] ids = itemFields[0].Split('#');
                                int storeID = GetStoreID;// int.Parse(ids[1].ToString());
                                int portalID = GetPortalID;// int.Parse(ids[2].ToString());
                                string userName = GetUsername;// ids[3].ToString();
                                int customerID = orderdata.ObjOrderDetails.CustomerID;// int.Parse(ids[4].ToString());
                                string sessionCode = orderdata.ObjOrderDetails.SessionCode;//ids[5].ToString();
                                //string pgid = ids[7].ToString();
                                string selectedCurrency = orderdata.ObjOrderDetails.CurrencyCode;//ids[8].ToString();
                                string itemids = itemFields.Replace('#', '&');
                                string couponCode = orderdata.ObjOrderDetails.CouponCode;//itemFields[2].Replace('#', '&');
                                string status = "Successful";
                                TransactionLogInfo tinfo = new TransactionLogInfo();
                                TransactionLog Tlog = new TransactionLog();
                                tinfo.TransactionID = transID;
                                //tinfo.AuthCode = aParam[1];
                                tinfo.TotalAmount = orderdata.ObjOrderDetails.GrandTotal;//decimal.Parse(aParam[5]);
                                tinfo.ResponseCode = orderdata.ObjOrderDetails.ResponseCode.ToString();//aParam[0];
                                //responsereason = aParam[1];
                                tinfo.ResponseReasonText = responsereason;
                                tinfo.OrderID = orderID;
                                tinfo.StoreID = storeID;
                                tinfo.PortalID = portalID;
                                tinfo.AddedBy = userName;
                                tinfo.CustomerID = customerID;
                                tinfo.SessionCode = sessionCode;
                                tinfo.PaymentGatewayID = orderdata.ObjOrderDetails.PaymentGatewayTypeID;//int.Parse(pgid);
                                tinfo.PaymentStatus = status;// aParam[1];
                                tinfo.PayerEmail = orderdata.ObjBillingAddressInfo.EmailAddress;//aParam[14];
                                tinfo.CreditCard = "";
                                tinfo.RecieverEmail = "info@sanchisolutions.com";
                                tinfo.CurrencyCode = selectedCurrency;
                                Tlog.SaveTransactionLog(tinfo);

                               
                                ParseIPN1(orderID, transID, status, storeID, portalID, userName, customerID, sessionCode);
                               

                                UpdateItemQuantity(itemids, couponCode, storeID, portalID, userName);
                                CartManageSQLProvider cms = new CartManageSQLProvider();
                                aspxCommonObj.CustomerID = customerID;
                                aspxCommonObj.SessionCode = sessionCode;
                                aspxCommonObj.StoreID = storeID;
                                aspxCommonObj.PortalID = portalID;
                                aspxCommonObj.CultureName = null;
                                aspxCommonObj.UserName = null;
                                cms.ClearCartAfterPayment(aspxCommonObj);
                                object amount = orderdata.ObjOrderDetails.GrandTotal;
                                lblTransaction.Text = transID;
                                lblPaymentMethod.Text = "Online Payment";
                                lblDateTime.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy ");
                                lblInvoice.Text = invoice;
                                lblOrderNo.Text = "#" + transID;
                                lblPaymentStatus.Text = status;
                                ClearAllSession();
                            }
                            else
                            {
                                // Response.Redirect(sageRedirectPath, false);
                                ClearAllSession();
                            }
                        //}
                    }
                    else
                    {
                        //string[] itemFields = aParam[22].Split('|');
                        for (int i = 0; i < orderdata.LstOrderItemsInfo.Count; i++)
                        {
                             item += orderdata.LstOrderItemsInfo[i].ItemID + "#" + orderdata.LstOrderItemsInfo[i].Quantity + "#"
                                + orderdata.LstOrderItemsInfo[i].OrderID + "#" + orderdata.LstOrderItemsInfo[i].Variants + ",";                            
                        }
                        string itemFields;// aParam[22].Split('|');
                        if (orderdata.LstOrderItemsInfo.Count > 1)
                        { itemFields = item.TrimEnd(','); }
                        else { itemFields = item; }
                        string responsereason = orderdata.ObjOrderDetails.ResponseReasonText;// aParam[1];

                        AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
                        //string[] ids = itemFields[0].Split('#');
                        int storeID = GetStoreID;// int.Parse(ids[1].ToString());
                        int portalID = GetPortalID;// int.Parse(ids[2].ToString());
                        string userName = GetUsername;// ids[3].ToString();
                        int customerID = orderdata.ObjOrderDetails.CustomerID;//int.Parse(ids[4].ToString());
                        string sessionCode = orderdata.ObjOrderDetails.SessionCode;//ids[5].ToString();
                        //string pgid = ids[7].ToString();
                        string selectedCurrency = orderdata.ObjOrderDetails.CurrencyCode;//ids[8].ToString();
                        string itemids = itemFields.Replace('#', '&');
                        string couponCode = orderdata.ObjOrderDetails.CouponCode;//itemFields[2].Replace('#', '&');
                        string status = "Unsuccessfull";
                        TransactionLogInfo tinfo = new TransactionLogInfo();
                        TransactionLog Tlog = new TransactionLog();

                        tinfo.TransactionID = transID;
                        //tinfo.AuthCode = aParam[1];
                        tinfo.TotalAmount = orderdata.ObjOrderDetails.GrandTotal;//decimal.Parse(aParam[5]);
                        tinfo.ResponseCode = orderdata.ObjOrderDetails.ResponseCode.ToString();//aParam[0];
                        //responsereason = aParam[1];
                        tinfo.ResponseReasonText = responsereason;
                        tinfo.OrderID = orderID;
                        tinfo.StoreID = storeID;
                        tinfo.PortalID = portalID;
                        tinfo.AddedBy = userName;
                        tinfo.CustomerID = customerID;
                        tinfo.SessionCode = sessionCode;
                        tinfo.PaymentGatewayID = orderdata.ObjOrderDetails.PaymentGatewayTypeID;//int.Parse(pgid);
                        tinfo.PaymentStatus = status;// aParam[1];
                        tinfo.PayerEmail = orderdata.ObjBillingAddressInfo.EmailAddress;//aParam[14];
                        tinfo.CreditCard = "";
                        tinfo.RecieverEmail = "info@diamonds4you.com";
                        tinfo.CurrencyCode = selectedCurrency;
                        Tlog.SaveTransactionLog(tinfo);
                        ParseIPN(orderID, transID, status, storeID, portalID, userName, customerID, sessionCode);
                        UpdateItemQuantity(itemids, couponCode, storeID, portalID, userName);
                        CartManageSQLProvider cms = new CartManageSQLProvider();
                        aspxCommonObj.CustomerID = customerID;
                        aspxCommonObj.SessionCode = sessionCode;
                        aspxCommonObj.StoreID = storeID;
                        aspxCommonObj.PortalID = portalID;
                        aspxCommonObj.CultureName = null;
                        aspxCommonObj.UserName = null;
                        cms.ClearCartAfterPayment(aspxCommonObj);
                        lblTransaction.Text = "Transaction Unsuccessfull for Payment.";
                        lblPaymentMethod.Text = "Online Payment";
                        lblDateTime.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy ");
                        lblInvoice.Text = invoice;
                        lblOrderNo.Text = "#" + transID;
                        lblPaymentStatus.Text = status;
                        ClearAllSession();
                    }
                }
            }
            catch (Exception ex)
            {
                //Response.Write("<span style='color:red'>" + ex.Message + "</span>");
                ProcessException(ex);
            }
        }
    }


    private void ClearAllSession()
    {
        Session.Remove("OrderCollection");

        if (Session["IsFreeShipping"] != null && Session["IsFreeShipping"] != "")
        {
            HttpContext.Current.Session.Remove("IsFreeShipping");

        }
        if (Session["OrderID"] != null && Session["OrderID"] != "")
        {
            HttpContext.Current.Session.Remove("OrderID");

        }
        if (Session["DiscountAmount"] != null && Session["DiscountAmount"] != "")
        {
            HttpContext.Current.Session.Remove("DiscountAmount");

        }
        if (Session["CouponCode"] != null && Session["CouponCode"] != "")
        {
            HttpContext.Current.Session.Remove("CouponCode");

        }
        if (Session["CouponApplied"] != null && Session["CouponApplied"] != "")
        {
            HttpContext.Current.Session.Remove("CouponApplied");
        }
        if (Session["DiscountAll"] != null && Session["DiscountAll"] != "")
        {
            HttpContext.Current.Session.Remove("DiscountAll");
        }
        if (Session["TaxAll"] != null && Session["TaxAll"] != "")
        {
            HttpContext.Current.Session.Remove("TaxAll");
        }
        if (Session["ShippingCostAll"] != null && Session["ShippingCostAll"] != "")
        {
            HttpContext.Current.Session.Remove("ShippingCostAll");
        }
        if (Session["GrandTotalAll"] != null && Session["GrandTotalAll"] != "")
        {
            HttpContext.Current.Session.Remove("GrandTotalAll");
        }
        if (Session["Gateway"] != null && Session["Gateway"] != "")
        {
            HttpContext.Current.Session.Remove("Gateway");
        }
        if (Session["transaction_id"] != null && Session["transaction_id"] != "")
        {
            HttpContext.Current.Session.Remove("transaction_id");
        }
    }
    public void UpdateItemQuantity(string itemIds, string coupon, int storeId, int portalId, string userName)
    {

        string[] ids = itemIds.Split(',');

        //id,quantity,isdownloadable
        for (int i = 0; i < ids.Length; i++)
        {
            if (ids[i].Contains("&"))
            {
                string[] itemdetails = ids[i].Split('&');
                //string[] coupondetails = coupon.Split('&');
                if (itemdetails[0] != null)
                {
                    var paraMeter = new List<KeyValuePair<string, object>>();
                    paraMeter.Add(new KeyValuePair<string, object>("@StoreID", storeId));
                    paraMeter.Add(new KeyValuePair<string, object>("@PortalID", portalId));
                    paraMeter.Add(new KeyValuePair<string, object>("@AddedBy", userName));
                    paraMeter.Add(new KeyValuePair<string, object>("@ItemID", itemdetails[0]));
                    paraMeter.Add(new KeyValuePair<string, object>("@Quantity", itemdetails[1]));
                    paraMeter.Add(new KeyValuePair<string, object>("@OrderID", itemdetails[2]));
                    paraMeter.Add(new KeyValuePair<string, object>("@CostVariantsIDs", itemdetails[3]));
                    SQLHandler sqlH = new SQLHandler();
                    sqlH.ExecuteNonQuery("[dbo].[usp_Aspx_UpdateItemQuantitybyOrder]", paraMeter);
                }

                //if (coupondetails.Length > 1)
                //{

                //    if (coupondetails[0] != null && coupondetails[1] != null)
                //    {
                //        var paraMeter = new List<KeyValuePair<string, object>>();
                //        paraMeter.Add(new KeyValuePair<string, object>("@CouponCode", coupondetails[0]));
                //        paraMeter.Add(new KeyValuePair<string, object>("@StoreID", storeId));
                //        paraMeter.Add(new KeyValuePair<string, object>("@PortalID", portalId));
                //        paraMeter.Add(new KeyValuePair<string, object>("@UserName", userName));
                //        paraMeter.Add(new KeyValuePair<string, object>("@CouponUsedCount", coupondetails[1]));
                //        paraMeter.Add(new KeyValuePair<string, object>("@OrderID", itemdetails[2]));
                //        SQLHandler sqlH = new SQLHandler();
                //        sqlH.ExecuteNonQuery("usp_Aspx_UpdateCouponUserRecord", paraMeter);
                //    }
                //}
            }


        }



    }
    public string Generatehash512(string text)
    {

        byte[] message = Encoding.UTF8.GetBytes(text);

        UnicodeEncoding UE = new UnicodeEncoding();
        byte[] hashValue;
        SHA512Managed hashString = new SHA512Managed();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;

    }


    public static void ParseIPN(int orderID, string transID, string status, int storeID, int portalID, string userName, int customerID, string sessionCode)
    {
        //MoneybookersHandler ph = new MoneybookersHandler();
        try
        {
            OrderDetailsCollection ot = new OrderDetailsCollection();
            OrderDetailsInfo odinfo = new OrderDetailsInfo();
            CartManageSQLProvider cms = new CartManageSQLProvider();
            CommonInfo cf = new CommonInfo();
            cf.StoreID = storeID;
            cf.PortalID = portalID;
            cf.AddedBy = userName;
            // UpdateOrderDetails
            AspxOrderDetails objad = new AspxOrderDetails();
            SQLHandler sqlH = new SQLHandler();
            // use split to split array we already have using "=" as delimiter
            // WcfSession ws = new WcfSession();
            odinfo.OrderID = orderID;//ws.GetSessionVariable("OrderID");
            odinfo.ResponseReasonText = status;
            odinfo.TransactionID = transID;
            ot.ObjOrderDetails = odinfo;
            ot.ObjCommonInfo = cf;
            odinfo.OrderStatusID = 5;
            objad.UpdateOrderDetails(ot);
            // UpdateItemQuantity
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void ParseIPN1(int orderID, string transID, string status, int storeID, int portalID, string userName, int customerID, string sessionCode)
    {
        //MoneybookersHandler ph = new MoneybookersHandler();
        try
        {
            OrderDetailsCollection ot = new OrderDetailsCollection();
            OrderDetailsInfo odinfo = new OrderDetailsInfo();
            CartManageSQLProvider cms = new CartManageSQLProvider();
            CommonInfo cf = new CommonInfo();
            cf.StoreID = storeID;
            cf.PortalID = portalID;
            cf.AddedBy = userName;
            // UpdateOrderDetails
            AspxOrderDetails objad = new AspxOrderDetails();
            SQLHandler sqlH = new SQLHandler();
            // use split to split array we already have using "=" as delimiter
            // WcfSession ws = new WcfSession();
            odinfo.OrderID = orderID;//ws.GetSessionVariable("OrderID");
            odinfo.ResponseReasonText = status;
            odinfo.TransactionID = transID;
            ot.ObjOrderDetails = odinfo;
            ot.ObjCommonInfo = cf;
            odinfo.OrderStatusID = 7;
            objad.UpdateOrderDetails(ot);
            // UpdateItemQuantity
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    //private string Base64Decode(string sBase64String)
    //{
    //    byte[] sBase64String_bytes =
    //    Convert.FromBase64String(sBase64String);
    //    return UnicodeEncoding.Default.GetString(sBase64String_bytes);
    //    //return UnicodeEncoding.ASCII.GetString(sBase64String_bytes);
    //}

    //public string base64Decode(string data)
    //{
    //    try
    //    {
    //        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
    //        System.Text.Decoder utf8Decode = encoder.GetDecoder();

    //        byte[] todecode_byte = Convert.FromBase64String(data);
    //        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
    //        char[] decoded_char = new char[charCount];
    //        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
    //        string result = new String(decoded_char);
    //        return result;
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Error in base64Decode" + e.Message);
    //    }
    //}
}
//public class RC4
//{
//    /**
//     * Get ASCII Integer Code
//     *
//     * @param char ch Character to get ASCII value for
//     * @access private
//     * @return int
//     */
//    private static int ord(char ch)
//    {
//        return (int)(Encoding.GetEncoding(1252).GetBytes(ch + "")[0]);
//    }
//    /**
//     * Get character representation of ASCII Code
//     *
//     * @param int i ASCII code
//     * @access private
//     * @return char
//     */
//    private static char chr(int i)
//    {
//        byte[] bytes = new byte[1];
//        bytes[0] = (byte)i;
//        return Encoding.GetEncoding(1252).GetString(bytes)[0];
//    }
//    /**
//     * Convert Hex to Binary (hex2bin)
//     *
//     * @param string packtype Rudimentary in this implementation
//     * @param string datastring Hex to be packed into Binary
//     * @access private
//     * @return string
//     */
//    private static string pack(string packtype, string datastring)
//    {
//        int i, j, datalength, packsize;
//        byte[] bytes;
//        char[] hex;
//        string tmp;

//        datalength = datastring.Length;
//        packsize = (datalength / 2) + (datalength % 2);
//        bytes = new byte[packsize];
//        hex = new char[2];

//        for (i = j = 0; i < datalength; i += 2)
//        {
//            hex[0] = datastring[i];
//            if (datalength - i == 1)
//                hex[1] = '0';
//            else
//                hex[1] = datastring[i + 1];
//            tmp = new string(hex, 0, 2);
//            try { bytes[j++] = byte.Parse(tmp, System.Globalization.NumberStyles.HexNumber); }
//            catch { } /* grin */
//        }
//        return Encoding.GetEncoding(1252).GetString(bytes);
//    }
//    /**
//     * Convert Binary to Hex (bin2hex)
//     *
//     * @param string bindata Binary data
//     * @access public
//     * @return string
//     */
//    public static string bin2hex(string bindata)
//    {
//        int i;
//        byte[] bytes = Encoding.GetEncoding(1252).GetBytes(bindata);
//        string hexString = "";
//        for (i = 0; i < bytes.Length; i++)
//        {
//            hexString += bytes[i].ToString("x2");
//        }
//        return hexString;
//    }
//    /**
//     * The symmetric encryption function
//     *
//     * @param string pwd Key to encrypt with (can be binary of hex)
//     * @param string data Content to be encrypted
//     * @param bool ispwdHex Key passed is in hexadecimal or not
//     * @access public
//     * @return string
//     */
//    public static string Encrypt(string pwd, string data, bool ispwdHex)
//    {
//        int a, i, j, k, tmp, pwd_length, data_length;
//        int[] key, box;
//        byte[] cipher;
//        //string cipher;

//        if (ispwdHex)
//            pwd = pack("H*", pwd); // valid input, please!
//        pwd_length = pwd.Length;
//        data_length = data.Length;
//        key = new int[256];
//        box = new int[256];
//        cipher = new byte[data.Length];
//        //cipher = "";

//        for (i = 0; i < 256; i++)
//        {
//            key[i] = ord(pwd[i % pwd_length]);
//            box[i] = i;
//        }
//        for (j = i = 0; i < 256; i++)
//        {
//            j = (j + box[i] + key[i]) % 256;
//            tmp = box[i];
//            box[i] = box[j];
//            box[j] = tmp;
//        }
//        for (a = j = i = 0; i < data_length; i++)
//        {
//            a = (a + 1) % 256;
//            j = (j + box[a]) % 256;
//            tmp = box[a];
//            box[a] = box[j];
//            box[j] = tmp;
//            k = box[((box[a] + box[j]) % 256)];
//            cipher[i] = (byte)(ord(data[i]) ^ k);
//            //cipher += chr(ord(data[i]) ^ k);
//        }
//        return Encoding.GetEncoding(1252).GetString(cipher);
//        //return cipher;
//    }
//    /**
//     * Decryption, recall encryption
//     *
//     * @param string pwd Key to decrypt with (can be binary of hex)
//     * @param string data Content to be decrypted
//     * @param bool ispwdHex Key passed is in hexadecimal or not
//     * @access public
//     * @return string
//     */
//    public static string Decrypt(string pwd, string data, bool ispwdHex)
//    {
//        return Encrypt(pwd, data, ispwdHex);
//    }
//}